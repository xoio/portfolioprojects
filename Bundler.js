const fs = require('fs');
const vimeo = require('./VimeoBuilder')
const marked = require('./marked');

marked.setOptions({
    renderer:new marked.Renderer(),
    gfm:true
})


//read directory for all folders
var dirs = fs.readdirSync(__dirname + "/public");

//filter out anything that has an extension and or is a hidden file
dirs = dirs.map(item => {
    if(item.indexOf('.') === -1){
        return item;
    }
}).filter(item => {
    if(item !== undefined){
        return item;
    }
});

// build out project information
var paths = [];

dirs.forEach(item => {

    var final_name = [];
    var desc = ""

    //does a media folder exist?
    var path = `${__dirname}/public/${item}/media`;
    var mediaExists = fs.existsSync(path);
    var id = "";

    // does a markdown project description exist?
    var descPath = `${__dirname}/public/${item}/description.md`;
    var descExists = fs.existsSync(descPath);


    //build the id of the project
    if(item.indexOf(" ") !== -1){
        // if id has a space, join with a dash
        id = item.split(" ").join("-").toLowerCase()
    }else {
        id = item;
    }

    // build the client facing name of the item
    if(item.indexOf(" ") !== -1){
        //make sure letters are capitalized
        var name_split = item.split(" ");
        for(var i = 0; i < name_split.length; ++i){
            var portion =  name_split[i];

            final_name.push(portion.charAt(0).toUpperCase() + portion.slice(1));
        }

    }else if(item.indexOf("-") !== -1) {

        //make sure letters are capitalized
        var name_split = item.split("-");
        for(var i = 0; i < name_split.length; ++i){
            var portion =  name_split[i];

            final_name.push(portion.charAt(0).toUpperCase() + portion.slice(1));
        }
    }else {
        final_name.push(item);
    }


    // compile description if it exists
    if(descExists){
        var contents = fs.readFileSync(descPath,'utf8');
        desc = marked(contents)
    }

    if(mediaExists){
        paths.push({
            name:final_name.join(" "),
            id:id,
            description:desc,
            path:`/${item}`,
            thumb:`/${item}/thumb.jpg`,
            media:fs.readdirSync(path)
        });
    }else{
        paths.push({
            name:final_name.join(" "),
            id:id,
            description:desc,
            path:`/${item}`,
            thumb:`/${item}/thumb.jpg`,
            media:[]
        });
    }
});

//////// THIRD PARTY STUFF //////////
/*
  Build out all the stuff with data we might need to fetch from third parties.
*/

var third_party = fs.readFileSync(`${__dirname}/public/thirdparty.json`,"utf8");
third_party = JSON.parse(third_party);

/**
 *  Filter out the info for each catetory. Just videos for now, but
 *  may add more later
 */

 var videos = third_party.videos;
 videos.forEach((video,index) => {

   vimeo.fetchData(video.id,function(data){
      paths.push(data)

      if(index === videos.length - 1){
        writeFinalData();
      }
   });

 })


function writeFinalData(){
  fs.writeFile(__dirname + '/public/data.json',JSON.stringify(paths),function(err){
      if(err){
          console.log(err);
      }
  })
}
