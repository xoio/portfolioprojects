The purpose of this branch is to hold all of the projects that I've done, as well as it's source(if allowed).
This way, it provides an easy place for backup as well as enables an api of sorts, to parse work.

<br/>

While it would be nice to at least get a thank-you or mention, 
all content under this repository should be considered MIT licenced and freely available for your own use unless
otherwise specified.

