const http = require('./httprequest')
const base = "http://vimeo.com" + "/api/oembed.json?url=https%3A//vimeo.com/";
const embedurl = "https://player.vimeo.com/video"
module.exports = {
  fetchData:function(id,cb){
    http.httpRequest(base + id,function(body){
      if(body.status !== "error"){
        var data = JSON.parse(body);

        var id = "";
        //check to see if the name has a space
        if(data.title.indexOf(" ") !== -1){
          id = data.title.split(" ").join("-").toLowerCase()
        }else {
          id = data.title;
        }

          // make sure urls are https
          var thumb = data.thumbnail_url.replace("http","https")

          var playThumb = data.thumbnail_url_with_play_button.replace("http","https");
        var info = {
          name:data.title,
            id:id,
            isVimeo:true,
            html:data.html,
            width:data.width,
            height:data.height,
            description:data.description,
            video_id:data.video_id,
            thumbnail:thumb,
            thumb:thumb,
            thumb_with_play_button:playThumb,
            url:data.uri,
            media:[`${embedurl}/${data.video_id}`]
        }
        cb(info);
      }
    });
  }
}
