const Hapi = require('hapi');

const server = new Hapi.Server({
    connections: {
        routes: {
            cors: true
        }
    }
});
server.connection({port: 3001})
const request = require('request');

server.register(require('inert'),(err) => {
    if(err){
        throw err;
    }
    server.route({
        method:'GET',
        path:'/newyear/{param*}',
        handler:{
            directory: {
                path:'./public/newyear'
            }
        }
    });


    server.route({
        method:'GET',
        path:'/blink-test/{param*}',
        handler:{
            directory: {
                path:'./public/blink-test'
            }
        }
    });

    server.route({
        method:'GET',
        path:'/projectlist',
        handler:function(req,reply){
            request('https://xoio.gitlab.io/projects/data.json',function(err,response,body){

                if(err) {
                    reply(err);
                }
                reply(JSON.parse(body));
            });
        }
    })

    server.route({
        method:'GET',
        path:'/',
        handler:function(req,reply){
            reply.file(`./public/index.html`);
        }
    })

    server.route({
        method:'GET',
        path:'/image/{project}/{path}',
        handler:function(req,reply){
            request(`https://xoio.gitlab.io/projects/${req.params.project}/${req.params.path}`).on('response',function(response){
                reply(response)
            })
        }
    })

    server.route({
        method:'GET',
        path:'/js/{filename}',
        handler:function(req,reply){
            reply.file(`./public/js/${req.params.filename}`);
        }
    });

    server.route({
        method:'GET',
        path:'/js/prod/{filename}',
        handler:function(req,reply){
            reply.file(`./public/js/prod/${req.params.filename}`);
        }
    });


    server.route({
        method:'GET',
        path:'/content/{param*}',
        handler:{
            directory: {
                path:'./public/content'
            }
        }
    });


    server.route({
        method:'GET',
        path:'/css/{filename}',
        handler:function(req,reply){
            reply.file(`./public/css/${req.params.filename}`);
        }
    })
})

server.start((err) => {
    if(err){
        throw err;
    }

    console.log('Server running at: ',server.info.uri);
})


