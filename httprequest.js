const http = require('http');

module.exports = {
  httpRequest:function(url,cb){
    http.get(url,function(res){
      if(res.statusCode === 200){
        var body = '';
        res.on('data',function(chunk){
          body += chunk;
        });

        res.on("end",function(){
          cb(body);
        })
      }
    }).on("error",function(err){
      console.log("HTTP Request Error : ",err);
    })
  }
}
