Boks was a project I helped put together back when I was an intern. 
I was responsible for crafting a pdf generating system that could generate a 
printable letter that parents could send to their child's school to help convince 
teachers to try and adopt more beneficial activities for kids. 
It's still up [here](http://boksquiz.org/)