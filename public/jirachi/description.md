Jirachi is a personal WebGL toolkit. It's designed to be minimal enough to use on it's own, but at the same time
written in a way that makes it simpler to include and use with other languages.
<br/>
<br/>
It also makes use of many yet-to-be released features for WebGL such as native Vertex Attribute Objects as well as floating point textures.
This has been something I've been wanting to do for quite some time, but haven't quite managed to make due for various reasons.
<br/>
<br/>
You can see the code [here](https://gitlab.com/xoio/jirachi) as well as see a small demo of it in use in the images and [here](http://xoio.co/daijobanai/index.html)