(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

window.jraDebug = false;
window.toggleDebug = function () {
    window.jraDebug = !window.jraDebug;
};

/**
 * Logs an error message, only when window.jraDebug is set to true;
 * @param message
 */
function logError(message) {
    let css = "background:red;color:white; padding:4px;";
    if (window.jraDebug) {
        console.log(`%c ${ message }`, css);
    }
}

/**
 * Checks the context to ensure it has the desired extension enabled
 * @param ctx {WebGLRenderingContext} the webgl context to check
 * @param extension {String} the name of the extension to look for
 */


/**
 * Logs a warning message, only when window.jraDebug is set to true
 * @param message
 */
function logWarn(message) {
    let css = "background:yellow;color:red; padding:4px;";
    if (window.jraDebug) {
        console.log(`%c ${ message }`, css);
    }
}

/**
 * Logs a regular console.log call, only when window.jraDebug is set to true
 * @param message
 */

var RendererFormat = function (options = { width: window.innerWidth, height: window.innerHeight }) {
    this.width = options.width;
    this.height = options.height;
    this.viewportX = 0;
    this.viewportY = 0;
    this.clearColor = [0, 0, 0, 1];
};

RendererFormat.prototype = {
    /**
     * Appends the canvas to the DOM.
     * @param {node} el the element you want to append to. By default will append to body
     */
    attachToScreen(el = document.body) {
        el.appendChild(this.canvas);
        return this;
    },

    /**
     * Enables an attribute to become instanced, provided that the GPU supports the extension.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     * @param divisor The divisor setting for that attribute. It is 1 by default which should essentially turn on instancing.
     */
    enableInstancedAttribute(attributeLoc, divisor = 1) {

        if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
            let ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, divisor);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },

    /**
     * Disables an attribute to become instanced.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     */
    disableInstancedAttribute(attributeLoc) {
        if (this.hasOwnProperty("ANGLE_instance_arrays")) {
            let ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, 0);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },
    /**
     * Runs the drawArraysInstanced command of the context. If the context is
     * webgl 1, it attempts to try and use the extension, if webgl 2, it runs the
     * regular command.
     * @param mode A GLenum specifying the type primitive to render, ie GL_TRIANGLE, etc..:
     * @param first {Number} a number specifying the starting index in the array of vector points.
     * @param count {Number} a number specifying the number of vertices
     * @param primcount {Number} a number specifying the number of instances to draw
     */
    drawInstancedArrays(mode, first, count, primcount) {
        if (!this.isWebGL2) {
            if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
                this.ANGLE_instanced_arrays.drawArraysInstancedANGLE(mode, first, count, primcount);
            } else {
                console.error("Unable to draw instanced geometry - extension is not available");
            }
        } else {
            this.drawArraysInstanced(mode, first, count, primcount);
        }
    },

    /**
     * Drawing function to use for instanced items that have indices
     * @param mode {Number} the drawing mode, gl.TRIANGLES, etc..
     * @param numElements {Number} the number of element to draw(aka the number of indices)
     * @param numInstances {Number} the number of instances of the object to draw
     * @param type {Number} the data type of the index data, defaults to gl.UNSIGNED_SHORT
     * @param offset {Number} A GLintptr specifying an offset in the element array buffer. Must be a valid multiple of the size of the given type.
     */
    drawInstancedElements(mode, numElements, numInstances, { type = UNSIGNED_SHORT, offset = 0 } = {}) {
        if (!this.isWebGL2) {
            if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
                this.ANGLE_instanced_arrays.drawElementsInstancedANGLE(mode, numElements, type, offset, numInstances);
            } else {
                console.error("Unable to draw instanced geometry - extension is not available");
            }
        } else {
            this.drawElementsInstanced(mode, numElements, type, offset, numInstances);
        }
    },

    /**
     * Sets the context to be fullscreen.
     * @param {function} customResizeCallback
     * @returns {RendererFormat}
     */
    setFullscreen(customResizeCallback = null) {
        let self = this;
        let gl = this;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        //set the viewport size
        this.setViewport();

        if (customResizeCallback) {
            window.addEventListener("resize", customResizeCallback);
        } else {
            window.addEventListener("resize", () => {
                this.canvas.width = window.innerWidth;
                this.canvas.height = window.innerHeight;
                this.setViewport();
            });
        }
        return this;
    },

    /**
     * Helper function for clearing the screen, clear with a clear color,
     * set the viewport and clear the depth and color buffer bits
     * @param {number} r the value for the red channel of the clear color.
     * @param {number} g the value for the green channel of the clear color.
     * @param {number} b the value for the blue channel of the clear color.
     * @param {number} a the value for the alpha channel
     */
    clearScreen(r = 0, g = 0, b = 0, a = 1) {
        let gl = this;
        this.clearColor(r, g, b, a);
        gl.viewport(this.viewportX, this.viewportY, this.canvas.width, this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Enable depth testing
     */
    enableDepth() {
        this.gl.enable(this.gl.DEPTH_TEST);
        return this;
    },

    /**
     * Disables Depth testing
     */
    disableDepth() {
        this.gl.disable(this.gl.DEPTH_TEST);
    },

    /**
     * Returns the maximum texture size that the current card
     * supports.
     */
    getMaxTextureSize() {
        return this.gl.getParameter(this.gl.MAX_TEXTURE_SIZE);
    },

    /**
     * Sets the viewport for the context
     * @param {number} x the x coordinate for the viewport
     * @param {number} y the y coordinate for the viewport
     * @param {number} width the width for the viewport
     * @param {number} height the height for the viewport
     */
    setViewport(x = 0, y = 0, width = window.innerWidth, height = window.innerHeight) {
        let gl = this;
        gl.viewport(x, y, width, height);
    }

};

/**
 * Get list of all the extensions we want for this
 * @param gl a webgl context
 * @returns {{}}
 */
function getExtensions(gl) {
    let exts = {};

    // common extensions we might want
    const extensions = ["OES_texture_float", "OES_vertex_array_object", "ANGLE_instanced_arrays", "OES_texture_half_float", "OES_texture_float_linear", "OES_texture_half_float_linear", "WEBGL_color_buffer_float", "EXT_color_buffer_half_float", "WEBGL_draw_buffers"];

    extensions.forEach(name => {
        // try getting the extension
        let ext = gl.getExtension(name);

        // if debugging is active, show warning message for any missing extensions
        if (ext === null) {
            logWarn(`Unable to get extension ${ name }, things might look weird or just plain fail`);
        }
        exts[name] = ext;
    });

    return exts;
}

/**
 * Creates a WebGLRendering context
 * @param node an optional node to build the context from. If nothing is provided, we generate a canvas
 * @param options any options for the context
 * @returns {*} the resulting WebGLRenderingContext
 */
function createContext(node = null, options = {}) {
    let el = node !== null ? node : document.createElement("canvas");
    let isWebgl2 = false;
    let defaults = {
        alpha: true,
        antialias: true,
        depth: true
    };

    // override any defaults if set
    Object.assign(options, defaults);

    // the possible context flags, try for webgl 2
    let types = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl"];

    // loop through trying different context settings.
    var ctx = types.map(type => {
        var tCtx = el.getContext(type);
        if (tCtx !== null) {
            if (type === "webgl2" || type === "experimental-webgl2") {
                isWebgl2 = true;
            }
            return tCtx;
        }
    }).filter(val => {
        if (val !== undefined) {
            return val;
        }
    });

    // make sure to note that this is a webgl 2 context
    if (isWebgl2) {
        ctx[0]["isWebGL2"] = true;
    }
    // just return 1 context
    return ctx[0];
}

/**
 * Sets up some WebGL constant values on top of the
 * window object for ease of use
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 */
function setupConstants(gl) {
    var constants = {
        "FLOAT": gl.FLOAT,
        "UNSIGNED_BYTE": gl.UNSIGNED_BYTE,
        "UNSIGNED_SHORT": gl.UNSIGNED_SHORT,
        "ARRAY_BUFFER": gl.ARRAY_BUFFER,
        "ELEMENT_BUFFER": gl.ELEMENT_ARRAY_BUFFER,
        "RGBA": gl.RGBA,
        "RGB": gl.RGB,
        "TEXTURE_2D": gl.TEXTURE_2D,
        "STATIC_DRAW": gl.STATIC_DRAW,
        "DYNAMIC_DRAW": gl.DYNAMIC_DRAW,
        "TRIANGLES": gl.TRIANGLES,
        "TRIANGLE_STRIP": gl.TRIANGLE_STRIP,
        "POINTS": gl.POINTS,
        "UNSIGNED_SHORT": gl.UNSIGNED_SHORT,
        "FRAMEBUFFER": gl.FRAMEBUFFER,
        "COLOR_ATTACHMENT0": gl.COLOR_ATTACHMENT0,

        // texture related
        "CLAMP_TO_EDGE": gl.CLAMP_TO_EDGE,
        "LINEAR": gl.LINEAR,
        "MAG_FILTER": gl.TEXTURE_MAG_FILTER,
        "MIN_FILTER": gl.TEXTURE_MIN_FILTER,
        "WRAP_S": gl.TEXTURE_WRAP_S,
        "WRAP_T": gl.TEXTURE_WRAP_T,
        "TEXTURE0": gl.TEXTURE0,
        "TEXTURE1": gl.TEXTURE1,
        "TEXTURE2": gl.TEXTURE2,

        // some math related stuff
        "PI": 3.14149,
        "2_PI": 3.14149 * 3.14149
    };

    if (!window.GL_CONSTANTS_SET) {
        for (var i in constants) {
            window[i] = constants[i];
        }
        window.GL_CONSTANTS_SET = true;
    }
}

/**
 * Builds the WebGLRendering context
 * @param canvas {DomElement} an optional canvas, if you'd rather use one already in the DOM
 * @param ctxOptions {Object} options for the context
 * @param getCommonExtensions {Bool} include the common extensions for doing neat things in WebGL 1
 */
function createRenderer(canvas = null, ctxOptions = {}, getCommonExtensions = true) {
    let gl = createContext(canvas, ctxOptions);
    var format = new RendererFormat();
    let ext = null;

    if (getCommonExtensions) {
        ext = getExtensions(gl);
    }

    //setup constants
    setupConstants(gl);

    // assign some convenience functions onto the gl context
    var newProps = Object.assign(gl.__proto__, format.__proto__);
    gl.__proto__ = newProps;

    // loop through and assign extensions onto the context as well
    for (var i in ext) {
        gl[i] = ext[i];
    }

    return gl;
}

/**
 * Works along the same lines as thi.ng's render function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param spec a model spec to render
 */

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var index$2 = createCommonjsModule(function (module) {
	'use strict';

	function ToObject(val) {
		if (val == null) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}

		return Object(val);
	}

	module.exports = Object.assign || function (target, source) {
		var from;
		var keys;
		var to = ToObject(target);

		for (var s = 1; s < arguments.length; s++) {
			from = arguments[s];
			keys = Object.keys(Object(from));

			for (var i = 0; i < keys.length; i++) {
				to[keys[i]] = from[keys[i]];
			}
		}

		return to;
	};
});

var cross = createCommonjsModule(function (module) {
    module.exports = cross;

    /**
     * Computes the cross product of two vec3's
     *
     * @param {vec3} out the receiving vector
     * @param {vec3} a the first operand
     * @param {vec3} b the second operand
     * @returns {vec3} out
     */
    function cross(out, a, b) {
        var ax = a[0],
            ay = a[1],
            az = a[2],
            bx = b[0],
            by = b[1],
            bz = b[2];

        out[0] = ay * bz - az * by;
        out[1] = az * bx - ax * bz;
        out[2] = ax * by - ay * bx;
        return out;
    }
});

var dot = createCommonjsModule(function (module) {
  module.exports = dot;

  /**
   * Calculates the dot product of two vec3's
   *
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {Number} dot product of a and b
   */
  function dot(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
  }
});

var subtract = createCommonjsModule(function (module) {
  module.exports = subtract;

  /**
   * Subtracts vector b from vector a
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {vec3} out
   */
  function subtract(out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    return out;
  }
});

var index$6 = createCommonjsModule(function (module) {
    var cross$$1 = cross;
    var dot$$1 = dot;
    var sub = subtract;

    var EPSILON = 0.000001;
    var edge1 = [0, 0, 0];
    var edge2 = [0, 0, 0];
    var tvec = [0, 0, 0];
    var pvec = [0, 0, 0];
    var qvec = [0, 0, 0];

    module.exports = intersectTriangle;

    function intersectTriangle(out, pt, dir, tri) {
        sub(edge1, tri[1], tri[0]);
        sub(edge2, tri[2], tri[0]);

        cross$$1(pvec, dir, edge2);
        var det = dot$$1(edge1, pvec);

        if (det < EPSILON) return null;
        sub(tvec, pt, tri[0]);
        var u = dot$$1(tvec, pvec);
        if (u < 0 || u > det) return null;
        cross$$1(qvec, tvec, edge1);
        var v = dot$$1(dir, qvec);
        if (v < 0 || u + v > det) return null;

        var t = dot$$1(edge2, qvec) / det;
        out[0] = pt[0] + t * dir[0];
        out[1] = pt[1] + t * dir[1];
        out[2] = pt[2] + t * dir[2];
        return out;
    }
});

var add = createCommonjsModule(function (module) {
  module.exports = add;

  /**
   * Adds two vec3's
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {vec3} out
   */
  function add(out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    return out;
  }
});

var scale = createCommonjsModule(function (module) {
  module.exports = scale;

  /**
   * Scales a vec3 by a scalar number
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the vector to scale
   * @param {Number} b amount to scale the vector by
   * @returns {vec3} out
   */
  function scale(out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    return out;
  }
});

var copy = createCommonjsModule(function (module) {
  module.exports = copy;

  /**
   * Copy the values from one vec3 to another
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the source vector
   * @returns {vec3} out
   */
  function copy(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
  }
});

var index$8 = createCommonjsModule(function (module) {
  var dot$$1 = dot;
  var add$$1 = add;
  var scale$$1 = scale;
  var copy$$1 = copy;

  module.exports = intersectRayPlane;

  var v0 = [0, 0, 0];

  function intersectRayPlane(out, origin, direction, normal, dist) {
    var denom = dot$$1(direction, normal);
    if (denom !== 0) {
      var t = -(dot$$1(origin, normal) + dist) / denom;
      if (t < 0) {
        return null;
      }
      scale$$1(v0, direction, t);
      return add$$1(out, origin, v0);
    } else if (dot$$1(normal, origin) + dist === 0) {
      return copy$$1(out, origin);
    } else {
      return null;
    }
  }
});

var squaredDistance = createCommonjsModule(function (module) {
    module.exports = squaredDistance;

    /**
     * Calculates the squared euclidian distance between two vec3's
     *
     * @param {vec3} a the first operand
     * @param {vec3} b the second operand
     * @returns {Number} squared distance between a and b
     */
    function squaredDistance(a, b) {
        var x = b[0] - a[0],
            y = b[1] - a[1],
            z = b[2] - a[2];
        return x * x + y * y + z * z;
    }
});

var scaleAndAdd = createCommonjsModule(function (module) {
  module.exports = scaleAndAdd;

  /**
   * Adds two vec3's after scaling the second operand by a scalar value
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @param {Number} scale the amount to scale b by before adding
   * @returns {vec3} out
   */
  function scaleAndAdd(out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    return out;
  }
});

var index$10 = createCommonjsModule(function (module) {
  var squaredDist = squaredDistance;
  var dot$$1 = dot;
  var sub = subtract;
  var scaleAndAdd$$1 = scaleAndAdd;
  var scale$$1 = scale;
  var add$$1 = add;

  var tmp = [0, 0, 0];

  module.exports = intersectRaySphere;
  function intersectRaySphere(out, origin, direction, center, radius) {
    sub(tmp, center, origin);
    var len = dot$$1(direction, tmp);
    if (len < 0) {
      // sphere is behind ray
      return null;
    }

    scaleAndAdd$$1(tmp, origin, direction, len);
    var dSq = squaredDist(center, tmp);
    var rSq = radius * radius;
    if (dSq > rSq) {
      return null;
    }

    scale$$1(out, direction, len - Math.sqrt(rSq - dSq));
    return add$$1(out, out, origin);
  }
});

var index$12 = createCommonjsModule(function (module) {
  module.exports = intersection;
  module.exports.distance = distance;

  function intersection(out, ro, rd, aabb) {
    var d = distance(ro, rd, aabb);
    if (d === Infinity) {
      out = null;
    } else {
      out = out || [];
      for (var i = 0; i < ro.length; i++) {
        out[i] = ro[i] + rd[i] * d;
      }
    }

    return out;
  }

  function distance(ro, rd, aabb) {
    var dims = ro.length;
    var lo = -Infinity;
    var hi = +Infinity;

    for (var i = 0; i < dims; i++) {
      var dimLo = (aabb[0][i] - ro[i]) / rd[i];
      var dimHi = (aabb[1][i] - ro[i]) / rd[i];

      if (dimLo > dimHi) {
        var tmp = dimLo;
        dimLo = dimHi;
        dimHi = tmp;
      }

      if (dimHi < lo || dimLo > hi) {
        return Infinity;
      }

      if (dimLo > lo) lo = dimLo;
      if (dimHi < hi) hi = dimHi;
    }

    return lo > hi ? Infinity : lo;
  }
});

var index$4 = createCommonjsModule(function (module) {
  var intersectRayTriangle = index$6;
  var intersectRayPlane = index$8;
  var intersectRaySphere = index$10;
  var intersectRayBox = index$12;
  var copy3 = copy;

  var tmpTriangle = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

  var tmp3 = [0, 0, 0];

  module.exports = Ray;
  function Ray(origin, direction) {
    this.origin = origin || [0, 0, 0];
    this.direction = direction || [0, 0, -1];
  }

  Ray.prototype.set = function (origin, direction) {
    this.origin = origin;
    this.direction = direction;
  };

  Ray.prototype.copy = function (other) {
    copy3(this.origin, other.origin);
    copy3(this.direction, other.direction);
  };

  Ray.prototype.clone = function () {
    var other = new Ray();
    other.copy(this);
    return other;
  };

  Ray.prototype.intersectsSphere = function (center, radius) {
    return intersectRaySphere(tmp3, this.origin, this.direction, center, radius);
  };

  Ray.prototype.intersectsPlane = function (normal, distance) {
    return intersectRayPlane(tmp3, this.origin, this.direction, normal, distance);
  };

  Ray.prototype.intersectsTriangle = function (triangle) {
    return intersectRayTriangle(tmp3, this.origin, this.direction, triangle);
  };

  Ray.prototype.intersectsBox = function (aabb) {
    return intersectRayBox(tmp3, this.origin, this.direction, aabb);
  };

  Ray.prototype.intersectsTriangleCell = function (cell, positions) {
    var a = cell[0],
        b = cell[1],
        c = cell[2];
    tmpTriangle[0] = positions[a];
    tmpTriangle[1] = positions[b];
    tmpTriangle[2] = positions[c];
    return this.intersectsTriangle(tmpTriangle);
  };
});

var transformMat4 = createCommonjsModule(function (module) {
  module.exports = transformMat4;

  /**
   * Transforms the vec4 with a mat4.
   *
   * @param {vec4} out the receiving vector
   * @param {vec4} a the vector to transform
   * @param {mat4} m matrix to transform with
   * @returns {vec4} out
   */
  function transformMat4(out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2],
        w = a[3];
    out[0] = m[0] * x + m[4] * y + m[8] * z + m[12] * w;
    out[1] = m[1] * x + m[5] * y + m[9] * z + m[13] * w;
    out[2] = m[2] * x + m[6] * y + m[10] * z + m[14] * w;
    out[3] = m[3] * x + m[7] * y + m[11] * z + m[15] * w;
    return out;
  }
});

var set = createCommonjsModule(function (module) {
  module.exports = set;

  /**
   * Set the components of a vec4 to the given values
   *
   * @param {vec4} out the receiving vector
   * @param {Number} x X component
   * @param {Number} y Y component
   * @param {Number} z Z component
   * @param {Number} w W component
   * @returns {vec4} out
   */
  function set(out, x, y, z, w) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    return out;
  }
});

var index$14 = createCommonjsModule(function (module) {
  var transformMat4$$1 = transformMat4;
  var set$$1 = set;

  var NEAR_RANGE = 0;
  var FAR_RANGE = 1;
  var tmp4 = [0, 0, 0, 0];

  module.exports = cameraProject;
  function cameraProject(out, vec, viewport, combinedProjView) {
    var vX = viewport[0],
        vY = viewport[1],
        vWidth = viewport[2],
        vHeight = viewport[3],
        n = NEAR_RANGE,
        f = FAR_RANGE;

    // convert: clip space -> NDC -> window coords
    // implicit 1.0 for w component
    set$$1(tmp4, vec[0], vec[1], vec[2], 1.0);

    // transform into clip space
    transformMat4$$1(tmp4, tmp4, combinedProjView);

    // now transform into NDC
    var w = tmp4[3];
    if (w !== 0) {
      // how to handle infinity here?
      tmp4[0] = tmp4[0] / w;
      tmp4[1] = tmp4[1] / w;
      tmp4[2] = tmp4[2] / w;
    }

    // and finally into window coordinates
    // the foruth component is (1/clip.w)
    // which is the same as gl_FragCoord.w
    out[0] = vX + vWidth / 2 * tmp4[0] + (0 + vWidth / 2);
    out[1] = vY + vHeight / 2 * tmp4[1] + (0 + vHeight / 2);
    out[2] = (f - n) / 2 * tmp4[2] + (f + n) / 2;
    out[3] = w === 0 ? 0 : 1 / w;
    return out;
  }
});

var projectMat4 = createCommonjsModule(function (module) {
  module.exports = project;

  /**
   * Multiplies the input vec by the specified matrix, 
   * applying a W divide, and stores the result in out 
   * vector. This is useful for projection,
   * e.g. unprojecting a 2D point into 3D space.
   *
   * @method  prj
   * @param {vec3} out the output vector
   * @param {vec3} vec the input vector to project
   * @param {mat4} m the 4x4 matrix to multiply with 
   * @return {vec3} the out vector
   */
  function project(out, vec, m) {
    var x = vec[0],
        y = vec[1],
        z = vec[2],
        a00 = m[0],
        a01 = m[1],
        a02 = m[2],
        a03 = m[3],
        a10 = m[4],
        a11 = m[5],
        a12 = m[6],
        a13 = m[7],
        a20 = m[8],
        a21 = m[9],
        a22 = m[10],
        a23 = m[11],
        a30 = m[12],
        a31 = m[13],
        a32 = m[14],
        a33 = m[15];

    var lw = 1 / (x * a03 + y * a13 + z * a23 + a33);

    out[0] = (x * a00 + y * a10 + z * a20 + a30) * lw;
    out[1] = (x * a01 + y * a11 + z * a21 + a31) * lw;
    out[2] = (x * a02 + y * a12 + z * a22 + a32) * lw;
    return out;
  }
});

var index$16 = createCommonjsModule(function (module) {
  var transform = projectMat4;

  module.exports = unproject;

  /**
   * Unproject a point from screen space to 3D space.
   * The point should have its x and y properties set to
   * 2D screen space, and the z either at 0 (near plane)
   * or 1 (far plane). The provided matrix is assumed to already
   * be combined, i.e. projection * view.
   *
   * After this operation, the out vector's [x, y, z] components will
   * represent the unprojected 3D coordinate.
   *
   * @param  {vec3} out               the output vector
   * @param  {vec3} vec               the 2D space vector to unproject
   * @param  {vec4} viewport          screen x, y, width and height in pixels
   * @param  {mat4} invProjectionView combined projection and view matrix
   * @return {vec3}                   the output vector
   */
  function unproject(out, vec, viewport, invProjectionView) {
    var viewX = viewport[0],
        viewY = viewport[1],
        viewWidth = viewport[2],
        viewHeight = viewport[3];

    var x = vec[0],
        y = vec[1],
        z = vec[2];

    x = x - viewX;
    y = viewHeight - y - 1;
    y = y - viewY;

    out[0] = 2 * x / viewWidth - 1;
    out[1] = 2 * y / viewHeight - 1;
    out[2] = 2 * z - 1;
    return transform(out, out, invProjectionView);
  }
});

var normalize = createCommonjsModule(function (module) {
    module.exports = normalize;

    /**
     * Normalize a vec3
     *
     * @param {vec3} out the receiving vector
     * @param {vec3} a vector to normalize
     * @returns {vec3} out
     */
    function normalize(out, a) {
        var x = a[0],
            y = a[1],
            z = a[2];
        var len = x * x + y * y + z * z;
        if (len > 0) {
            //TODO: evaluate use of glm_invsqrt here?
            len = 1 / Math.sqrt(len);
            out[0] = a[0] * len;
            out[1] = a[1] * len;
            out[2] = a[2] * len;
        }
        return out;
    }
});

var cameraLookAt = createCommonjsModule(function (module) {
  // could be modularized...
  var cross$$1 = cross;
  var sub = subtract;
  var normalize$$1 = normalize;
  var copy$$1 = copy;
  var dot$$1 = dot;
  var scale$$1 = scale;

  var tmp = [0, 0, 0];
  var epsilon = 0.000000001;

  // modifies direction & up vectors in place
  module.exports = function (direction, up, position, target) {
    sub(tmp, target, position);
    normalize$$1(tmp, tmp);
    var isZero = tmp[0] === 0 && tmp[1] === 0 && tmp[2] === 0;
    if (!isZero) {
      var d = dot$$1(tmp, up);
      if (Math.abs(d - 1) < epsilon) {
        // collinear
        scale$$1(up, direction, -1);
      } else if (Math.abs(d + 1) < epsilon) {
        // collinear opposite
        copy$$1(up, direction);
      }
      copy$$1(direction, tmp);

      // normalize up vector
      cross$$1(tmp, direction, up);
      normalize$$1(tmp, tmp);

      cross$$1(up, tmp, direction);
      normalize$$1(up, up);
    }
  };
});

var set$2 = createCommonjsModule(function (module) {
  module.exports = set;

  /**
   * Set the components of a vec3 to the given values
   *
   * @param {vec3} out the receiving vector
   * @param {Number} x X component
   * @param {Number} y Y component
   * @param {Number} z Z component
   * @returns {vec3} out
   */
  function set(out, x, y, z) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
  }
});

var index$18 = createCommonjsModule(function (module) {
  var unproject = index$16;
  var set = set$2;
  var sub = subtract;
  var normalize$$1 = normalize;

  module.exports = createPickRay;
  function createPickRay(origin, direction, point, viewport, invProjView) {
    set(origin, point[0], point[1], 0);
    set(direction, point[0], point[1], 1);
    unproject(origin, origin, viewport, invProjView);
    unproject(direction, direction, viewport, invProjView);
    sub(direction, direction, origin);
    normalize$$1(direction, direction);
  }
});

var multiply = createCommonjsModule(function (module) {
    module.exports = multiply;

    /**
     * Multiplies two mat4's
     *
     * @param {mat4} out the receiving matrix
     * @param {mat4} a the first operand
     * @param {mat4} b the second operand
     * @returns {mat4} out
     */
    function multiply(out, a, b) {
        var a00 = a[0],
            a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a10 = a[4],
            a11 = a[5],
            a12 = a[6],
            a13 = a[7],
            a20 = a[8],
            a21 = a[9],
            a22 = a[10],
            a23 = a[11],
            a30 = a[12],
            a31 = a[13],
            a32 = a[14],
            a33 = a[15];

        // Cache only the current line of the second matrix
        var b0 = b[0],
            b1 = b[1],
            b2 = b[2],
            b3 = b[3];
        out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
        out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
        out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
        out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
        return out;
    }
});

var invert = createCommonjsModule(function (module) {
    module.exports = invert;

    /**
     * Inverts a mat4
     *
     * @param {mat4} out the receiving matrix
     * @param {mat4} a the source matrix
     * @returns {mat4} out
     */
    function invert(out, a) {
        var a00 = a[0],
            a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a10 = a[4],
            a11 = a[5],
            a12 = a[6],
            a13 = a[7],
            a20 = a[8],
            a21 = a[9],
            a22 = a[10],
            a23 = a[11],
            a30 = a[12],
            a31 = a[13],
            a32 = a[14],
            a33 = a[15],
            b00 = a00 * a11 - a01 * a10,
            b01 = a00 * a12 - a02 * a10,
            b02 = a00 * a13 - a03 * a10,
            b03 = a01 * a12 - a02 * a11,
            b04 = a01 * a13 - a03 * a11,
            b05 = a02 * a13 - a03 * a12,
            b06 = a20 * a31 - a21 * a30,
            b07 = a20 * a32 - a22 * a30,
            b08 = a20 * a33 - a23 * a30,
            b09 = a21 * a32 - a22 * a31,
            b10 = a21 * a33 - a23 * a31,
            b11 = a22 * a33 - a23 * a32,


        // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        if (!det) {
            return null;
        }
        det = 1.0 / det;

        out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
        out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
        out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
        out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
        out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
        out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
        out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
        out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
        out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
        out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
        out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
        out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
        out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
        out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
        out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
        out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

        return out;
    }
});

var identity = createCommonjsModule(function (module) {
    module.exports = identity;

    /**
     * Set a mat4 to the identity matrix
     *
     * @param {mat4} out the receiving matrix
     * @returns {mat4} out
     */
    function identity(out) {
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = 1;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = 1;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }
});

var cameraBase = createCommonjsModule(function (module) {
  var assign = index$2;
  var Ray = index$4;

  var cameraProject = index$14;
  var cameraUnproject = index$16;
  var cameraLookAt$$1 = cameraLookAt;
  var cameraPickRay = index$18;

  var add$$1 = add;
  var multiply4x4 = multiply;
  var invert4x4 = invert;
  var identity4x4 = identity;
  var setVec3 = set$2;

  // this could also be useful for a orthographic camera
  module.exports = function cameraBase(opt) {
    opt = opt || {};

    var camera = {
      projection: identity4x4([]),
      view: identity4x4([]),
      position: opt.position || [0, 0, 0],
      direction: opt.direction || [0, 0, -1],
      up: opt.up || [0, 1, 0],
      viewport: opt.viewport || [-1, -1, 1, 1],
      projView: identity4x4([]),
      invProjView: identity4x4([])
    };

    function update() {
      multiply4x4(camera.projView, camera.projection, camera.view);
      var valid = invert4x4(camera.invProjView, camera.projView);
      if (!valid) {
        throw new Error('camera projection * view is non-invertible');
      }
    }

    function lookAt(target) {
      cameraLookAt$$1(camera.direction, camera.up, camera.position, target);
      return camera;
    }

    function identity$$1() {
      setVec3(camera.position, 0, 0, 0);
      setVec3(camera.direction, 0, 0, -1);
      setVec3(camera.up, 0, 1, 0);
      identity4x4(camera.view);
      identity4x4(camera.projection);
      identity4x4(camera.projView);
      identity4x4(camera.invProjView);
      return camera;
    }

    function translate(vec) {
      add$$1(camera.position, camera.position, vec);
      return camera;
    }

    function createPickingRay(mouse) {
      var ray = new Ray();
      cameraPickRay(ray.origin, ray.direction, mouse, camera.viewport, camera.invProjView);
      return ray;
    }

    function project(point) {
      return cameraProject([], point, camera.viewport, camera.projView);
    }

    function unproject(point) {
      return cameraUnproject([], point, camera.viewport, camera.invProjView);
    }

    return assign(camera, {
      translate: translate,
      identity: identity$$1,
      lookAt: lookAt,
      createPickingRay: createPickingRay,
      update: update,
      project: project,
      unproject: unproject
    });
  };
});

var index$20 = createCommonjsModule(function (module) {
    module.exports = function () {
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] !== undefined) return arguments[i];
        }
    };
});

var perspective = createCommonjsModule(function (module) {
    module.exports = perspective;

    /**
     * Generates a perspective projection matrix with the given bounds
     *
     * @param {mat4} out mat4 frustum matrix will be written into
     * @param {number} fovy Vertical field of view in radians
     * @param {number} aspect Aspect ratio. typically viewport width/height
     * @param {number} near Near bound of the frustum
     * @param {number} far Far bound of the frustum
     * @returns {mat4} out
     */
    function perspective(out, fovy, aspect, near, far) {
        var f = 1.0 / Math.tan(fovy / 2),
            nf = 1 / (near - far);
        out[0] = f / aspect;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = f;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = (far + near) * nf;
        out[11] = -1;
        out[12] = 0;
        out[13] = 0;
        out[14] = 2 * far * near * nf;
        out[15] = 0;
        return out;
    }
});

var lookAt = createCommonjsModule(function (module) {
    var identity$$1 = identity;

    module.exports = lookAt;

    /**
     * Generates a look-at matrix with the given eye position, focal point, and up axis
     *
     * @param {mat4} out mat4 frustum matrix will be written into
     * @param {vec3} eye Position of the viewer
     * @param {vec3} center Point the viewer is looking at
     * @param {vec3} up vec3 pointing up
     * @returns {mat4} out
     */
    function lookAt(out, eye, center, up) {
        var x0,
            x1,
            x2,
            y0,
            y1,
            y2,
            z0,
            z1,
            z2,
            len,
            eyex = eye[0],
            eyey = eye[1],
            eyez = eye[2],
            upx = up[0],
            upy = up[1],
            upz = up[2],
            centerx = center[0],
            centery = center[1],
            centerz = center[2];

        if (Math.abs(eyex - centerx) < 0.000001 && Math.abs(eyey - centery) < 0.000001 && Math.abs(eyez - centerz) < 0.000001) {
            return identity$$1(out);
        }

        z0 = eyex - centerx;
        z1 = eyey - centery;
        z2 = eyez - centerz;

        len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
        z0 *= len;
        z1 *= len;
        z2 *= len;

        x0 = upy * z2 - upz * z1;
        x1 = upz * z0 - upx * z2;
        x2 = upx * z1 - upy * z0;
        len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
        if (!len) {
            x0 = 0;
            x1 = 0;
            x2 = 0;
        } else {
            len = 1 / len;
            x0 *= len;
            x1 *= len;
            x2 *= len;
        }

        y0 = z1 * x2 - z2 * x1;
        y1 = z2 * x0 - z0 * x2;
        y2 = z0 * x1 - z1 * x0;

        len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
        if (!len) {
            y0 = 0;
            y1 = 0;
            y2 = 0;
        } else {
            len = 1 / len;
            y0 *= len;
            y1 *= len;
            y2 *= len;
        }

        out[0] = x0;
        out[1] = y0;
        out[2] = z0;
        out[3] = 0;
        out[4] = x1;
        out[5] = y1;
        out[6] = z1;
        out[7] = 0;
        out[8] = x2;
        out[9] = y2;
        out[10] = z2;
        out[11] = 0;
        out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
        out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
        out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
        out[15] = 1;

        return out;
    }
});

var cameraPerspective = createCommonjsModule(function (module) {
  var create = cameraBase;
  var assign = index$2;
  var defined = index$20;

  var perspective$$1 = perspective;
  var lookAt4x4 = lookAt;
  var add$$1 = add;

  module.exports = function cameraPerspective(opt) {
    opt = opt || {};

    var camera = create(opt);
    camera.fov = defined(opt.fov, Math.PI / 4);
    camera.near = defined(opt.near, 1);
    camera.far = defined(opt.far, 100);

    var center = [0, 0, 0];

    var updateCombined = camera.update;

    function update() {
      var aspect = camera.viewport[2] / camera.viewport[3];

      // build projection matrix
      perspective$$1(camera.projection, camera.fov, aspect, Math.abs(camera.near), Math.abs(camera.far));

      // build view matrix
      add$$1(center, camera.position, camera.direction);
      lookAt4x4(camera.view, camera.position, center, camera.up);

      // update projection * view and invert
      updateCombined();
      return camera;
    }

    // set it up initially from constructor options
    update();
    return assign(camera, {
      update: update
    });
  };
});

var index$1 = createCommonjsModule(function (module) {
  module.exports = cameraPerspective;
});

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class Common utilities
 * @name glMatrix
 */
var glMatrix = {};

// Configuration Constants
glMatrix.EPSILON = 0.000001;
glMatrix.ARRAY_TYPE = typeof Float32Array !== 'undefined' ? Float32Array : Array;
glMatrix.RANDOM = Math.random;
glMatrix.ENABLE_SIMD = false;

// Capability detection
glMatrix.SIMD_AVAILABLE = glMatrix.ARRAY_TYPE === window.Float32Array && 'SIMD' in window;
glMatrix.USE_SIMD = glMatrix.ENABLE_SIMD && glMatrix.SIMD_AVAILABLE;

/**
 * Sets the type of array used when creating new vectors and matrices
 *
 * @param {Type} type Array type, such as Float32Array or Array
 */
glMatrix.setMatrixArrayType = function (type) {
  glMatrix.ARRAY_TYPE = type;
};

var degree = Math.PI / 180;

/**
 * Convert Degree To Radian
 *
 * @param {Number} a Angle in Degrees
 */
glMatrix.toRadian = function (a) {
  return a * degree;
};

/**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance of glMatrix.EPSILON (an absolute tolerance is used for values less
 * than or equal to 1.0, and a relative tolerance is used for larger values)
 *
 * @param {Number} a The first number to test.
 * @param {Number} b The second number to test.
 * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
 */
glMatrix.equals = function (a, b) {
  return Math.abs(a - b) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a), Math.abs(b));
};

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class 4x4 Matrix
 * @name mat4
 */
var mat4 = {
    scalar: {},
    SIMD: {}
};

/**
 * Creates a new identity mat4
 *
 * @returns {mat4} a new 4x4 matrix
 */
mat4.create = function () {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a new mat4 initialized with values from an existing matrix
 *
 * @param {mat4} a matrix to clone
 * @returns {mat4} a new 4x4 matrix
 */
mat4.clone = function (a) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Copy the values from one mat4 to another
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.copy = function (out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Create a new mat4 with the given values
 *
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} A new mat4
 */
mat4.fromValues = function (m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set the components of a mat4 to the given values
 *
 * @param {mat4} out the receiving matrix
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} out
 */
mat4.set = function (out, m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set a mat4 to the identity matrix
 *
 * @param {mat4} out the receiving matrix
 * @returns {mat4} out
 */
mat4.identity = function (out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Transpose the values of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.transpose = function (out, a) {
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out === a) {
        var a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a12 = a[6],
            a13 = a[7],
            a23 = a[11];

        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a01;
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a02;
        out[9] = a12;
        out[11] = a[14];
        out[12] = a03;
        out[13] = a13;
        out[14] = a23;
    } else {
        out[0] = a[0];
        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a[1];
        out[5] = a[5];
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a[2];
        out[9] = a[6];
        out[10] = a[10];
        out[11] = a[14];
        out[12] = a[3];
        out[13] = a[7];
        out[14] = a[11];
        out[15] = a[15];
    }

    return out;
};

/**
 * Transpose the values of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.transpose = function (out, a) {
    var a0, a1, a2, a3, tmp01, tmp23, out0, out1, out2, out3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    out0 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out1 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 0, out0);
    SIMD.Float32x4.store(out, 4, out1);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    out2 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out3 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 8, out2);
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Transpse a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.transpose = glMatrix.USE_SIMD ? mat4.SIMD.transpose : mat4.scalar.transpose;

/**
 * Inverts a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.invert = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,


    // Calculate the determinant
    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
};

/**
 * Inverts a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.invert = function (out, a) {
    var row0,
        row1,
        row2,
        row3,
        tmp1,
        minor0,
        minor1,
        minor2,
        minor3,
        det,
        a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12);

    // Compute matrix adjugate
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    // Compute matrix determinant
    det = SIMD.Float32x4.mul(row0, minor0);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 2, 3, 0, 1), det);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 1, 0, 3, 2), det);
    tmp1 = SIMD.Float32x4.reciprocalApproximation(det);
    det = SIMD.Float32x4.sub(SIMD.Float32x4.add(tmp1, tmp1), SIMD.Float32x4.mul(det, SIMD.Float32x4.mul(tmp1, tmp1)));
    det = SIMD.Float32x4.swizzle(det, 0, 0, 0, 0);
    if (!det) {
        return null;
    }

    // Compute matrix inverse
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(det, minor0));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(det, minor1));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(det, minor2));
    SIMD.Float32x4.store(out, 12, SIMD.Float32x4.mul(det, minor3));
    return out;
};

/**
 * Inverts a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.invert = glMatrix.USE_SIMD ? mat4.SIMD.invert : mat4.scalar.invert;

/**
 * Calculates the adjugate of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.adjoint = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    out[0] = a11 * (a22 * a33 - a23 * a32) - a21 * (a12 * a33 - a13 * a32) + a31 * (a12 * a23 - a13 * a22);
    out[1] = -(a01 * (a22 * a33 - a23 * a32) - a21 * (a02 * a33 - a03 * a32) + a31 * (a02 * a23 - a03 * a22));
    out[2] = a01 * (a12 * a33 - a13 * a32) - a11 * (a02 * a33 - a03 * a32) + a31 * (a02 * a13 - a03 * a12);
    out[3] = -(a01 * (a12 * a23 - a13 * a22) - a11 * (a02 * a23 - a03 * a22) + a21 * (a02 * a13 - a03 * a12));
    out[4] = -(a10 * (a22 * a33 - a23 * a32) - a20 * (a12 * a33 - a13 * a32) + a30 * (a12 * a23 - a13 * a22));
    out[5] = a00 * (a22 * a33 - a23 * a32) - a20 * (a02 * a33 - a03 * a32) + a30 * (a02 * a23 - a03 * a22);
    out[6] = -(a00 * (a12 * a33 - a13 * a32) - a10 * (a02 * a33 - a03 * a32) + a30 * (a02 * a13 - a03 * a12));
    out[7] = a00 * (a12 * a23 - a13 * a22) - a10 * (a02 * a23 - a03 * a22) + a20 * (a02 * a13 - a03 * a12);
    out[8] = a10 * (a21 * a33 - a23 * a31) - a20 * (a11 * a33 - a13 * a31) + a30 * (a11 * a23 - a13 * a21);
    out[9] = -(a00 * (a21 * a33 - a23 * a31) - a20 * (a01 * a33 - a03 * a31) + a30 * (a01 * a23 - a03 * a21));
    out[10] = a00 * (a11 * a33 - a13 * a31) - a10 * (a01 * a33 - a03 * a31) + a30 * (a01 * a13 - a03 * a11);
    out[11] = -(a00 * (a11 * a23 - a13 * a21) - a10 * (a01 * a23 - a03 * a21) + a20 * (a01 * a13 - a03 * a11));
    out[12] = -(a10 * (a21 * a32 - a22 * a31) - a20 * (a11 * a32 - a12 * a31) + a30 * (a11 * a22 - a12 * a21));
    out[13] = a00 * (a21 * a32 - a22 * a31) - a20 * (a01 * a32 - a02 * a31) + a30 * (a01 * a22 - a02 * a21);
    out[14] = -(a00 * (a11 * a32 - a12 * a31) - a10 * (a01 * a32 - a02 * a31) + a30 * (a01 * a12 - a02 * a11));
    out[15] = a00 * (a11 * a22 - a12 * a21) - a10 * (a01 * a22 - a02 * a21) + a20 * (a01 * a12 - a02 * a11);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.adjoint = function (out, a) {
    var a0, a1, a2, a3;
    var row0, row1, row2, row3;
    var tmp1;
    var minor0, minor1, minor2, minor3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    // Transpose the source matrix.  Sort of.  Not a true transpose operation
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    SIMD.Float32x4.store(out, 0, minor0);
    SIMD.Float32x4.store(out, 4, minor1);
    SIMD.Float32x4.store(out, 8, minor2);
    SIMD.Float32x4.store(out, 12, minor3);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.adjoint = glMatrix.USE_SIMD ? mat4.SIMD.adjoint : mat4.scalar.adjoint;

/**
 * Calculates the determinant of a mat4
 *
 * @param {mat4} a the source matrix
 * @returns {Number} determinant of a
 */
mat4.determinant = function (a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
};

/**
 * Multiplies two mat4's explicitly using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand, must be a Float32Array
 * @param {mat4} b the second operand, must be a Float32Array
 * @returns {mat4} out
 */
mat4.SIMD.multiply = function (out, a, b) {
    var a0 = SIMD.Float32x4.load(a, 0);
    var a1 = SIMD.Float32x4.load(a, 4);
    var a2 = SIMD.Float32x4.load(a, 8);
    var a3 = SIMD.Float32x4.load(a, 12);

    var b0 = SIMD.Float32x4.load(b, 0);
    var out0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 0, out0);

    var b1 = SIMD.Float32x4.load(b, 4);
    var out1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 4, out1);

    var b2 = SIMD.Float32x4.load(b, 8);
    var out2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 8, out2);

    var b3 = SIMD.Float32x4.load(b, 12);
    var out3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Multiplies two mat4's explicitly not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.scalar.multiply = function (out, a, b) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    // Cache only the current line of the second matrix
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3];
    out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
    out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
    out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
    out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
    return out;
};

/**
 * Multiplies two mat4's using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.multiply = glMatrix.USE_SIMD ? mat4.SIMD.multiply : mat4.scalar.multiply;

/**
 * Alias for {@link mat4.multiply}
 * @function
 */
mat4.mul = mat4.multiply;

/**
 * Translate a mat4 by the given vector not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.scalar.translate = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2],
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23;

    if (a === out) {
        out[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        out[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        out[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        out[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
    } else {
        a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
        a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
        a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

        out[0] = a00;out[1] = a01;out[2] = a02;out[3] = a03;
        out[4] = a10;out[5] = a11;out[6] = a12;out[7] = a13;
        out[8] = a20;out[9] = a21;out[10] = a22;out[11] = a23;

        out[12] = a00 * x + a10 * y + a20 * z + a[12];
        out[13] = a01 * x + a11 * y + a21 * z + a[13];
        out[14] = a02 * x + a12 * y + a22 * z + a[14];
        out[15] = a03 * x + a13 * y + a23 * z + a[15];
    }

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.SIMD.translate = function (out, a, v) {
    var a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12),
        vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    if (a !== out) {
        out[0] = a[0];out[1] = a[1];out[2] = a[2];out[3] = a[3];
        out[4] = a[4];out[5] = a[5];out[6] = a[6];out[7] = a[7];
        out[8] = a[8];out[9] = a[9];out[10] = a[10];out[11] = a[11];
    }

    a0 = SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0));
    a1 = SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1));
    a2 = SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2));

    var t0 = SIMD.Float32x4.add(a0, SIMD.Float32x4.add(a1, SIMD.Float32x4.add(a2, a3)));
    SIMD.Float32x4.store(out, 12, t0);

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.translate = glMatrix.USE_SIMD ? mat4.SIMD.translate : mat4.scalar.translate;

/**
 * Scales the mat4 by the dimensions in the given vec3 not using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.scalar.scale = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2];

    out[0] = a[0] * x;
    out[1] = a[1] * x;
    out[2] = a[2] * x;
    out[3] = a[3] * x;
    out[4] = a[4] * y;
    out[5] = a[5] * y;
    out[6] = a[6] * y;
    out[7] = a[7] * y;
    out[8] = a[8] * z;
    out[9] = a[9] * z;
    out[10] = a[10] * z;
    out[11] = a[11] * z;
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.SIMD.scale = function (out, a, v) {
    var a0, a1, a2;
    var vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    a0 = SIMD.Float32x4.load(a, 0);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0)));

    a1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1)));

    a2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2)));

    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 */
mat4.scale = glMatrix.USE_SIMD ? mat4.SIMD.scale : mat4.scalar.scale;

/**
 * Rotates a mat4 by the given angle around the given axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.rotate = function (out, a, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t,
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23,
        b00,
        b01,
        b02,
        b10,
        b11,
        b12,
        b20,
        b21,
        b22;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
    a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
    a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c;b01 = y * x * t + z * s;b02 = z * x * t - y * s;
    b10 = x * y * t - z * s;b11 = y * y * t + c;b12 = z * y * t + x * s;
    b20 = x * z * t + y * s;b21 = y * z * t - x * s;b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    out[0] = a00 * b00 + a10 * b01 + a20 * b02;
    out[1] = a01 * b00 + a11 * b01 + a21 * b02;
    out[2] = a02 * b00 + a12 * b01 + a22 * b02;
    out[3] = a03 * b00 + a13 * b01 + a23 * b02;
    out[4] = a00 * b10 + a10 * b11 + a20 * b12;
    out[5] = a01 * b10 + a11 * b11 + a21 * b12;
    out[6] = a02 * b10 + a12 * b11 + a22 * b12;
    out[7] = a03 * b10 + a13 * b11 + a23 * b12;
    out[8] = a00 * b20 + a10 * b21 + a20 * b22;
    out[9] = a01 * b20 + a11 * b21 + a21 * b22;
    out[10] = a02 * b20 + a12 * b21 + a22 * b22;
    out[11] = a03 * b20 + a13 * b21 + a23 * b22;

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateX = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[4] = a10 * c + a20 * s;
    out[5] = a11 * c + a21 * s;
    out[6] = a12 * c + a22 * s;
    out[7] = a13 * c + a23 * s;
    out[8] = a20 * c - a10 * s;
    out[9] = a21 * c - a11 * s;
    out[10] = a22 * c - a12 * s;
    out[11] = a23 * c - a13 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateX = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_1 = SIMD.Float32x4.load(a, 4);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_2, c), SIMD.Float32x4.mul(a_1, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD if availabe and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateX = glMatrix.USE_SIMD ? mat4.SIMD.rotateX : mat4.scalar.rotateX;

/**
 * Rotates a matrix by the given angle around the Y axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateY = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c - a20 * s;
    out[1] = a01 * c - a21 * s;
    out[2] = a02 * c - a22 * s;
    out[3] = a03 * c - a23 * s;
    out[8] = a00 * s + a20 * c;
    out[9] = a01 * s + a21 * c;
    out[10] = a02 * s + a22 * c;
    out[11] = a03 * s + a23 * c;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateY = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, s), SIMD.Float32x4.mul(a_2, c)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateY = glMatrix.USE_SIMD ? mat4.SIMD.rotateY : mat4.scalar.rotateY;

/**
 * Rotates a matrix by the given angle around the Z axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateZ = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c + a10 * s;
    out[1] = a01 * c + a11 * s;
    out[2] = a02 * c + a12 * s;
    out[3] = a03 * c + a13 * s;
    out[4] = a10 * c - a00 * s;
    out[5] = a11 * c - a01 * s;
    out[6] = a12 * c - a02 * s;
    out[7] = a13 * c - a03 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateZ = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_1, s)));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_0, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateZ = glMatrix.USE_SIMD ? mat4.SIMD.rotateZ : mat4.scalar.rotateZ;

/**
 * Creates a matrix from a vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromTranslation = function (out, v) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a vector scaling
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.scale(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Scaling vector
 * @returns {mat4} out
 */
mat4.fromScaling = function (out, v) {
    out[0] = v[0];
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = v[1];
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = v[2];
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a given angle around a given axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotate(dest, dest, rad, axis);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.fromRotation = function (out, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    // Perform rotation-specific matrix multiplication
    out[0] = x * x * t + c;
    out[1] = y * x * t + z * s;
    out[2] = z * x * t - y * s;
    out[3] = 0;
    out[4] = x * y * t - z * s;
    out[5] = y * y * t + c;
    out[6] = z * y * t + x * s;
    out[7] = 0;
    out[8] = x * z * t + y * s;
    out[9] = y * z * t - x * s;
    out[10] = z * z * t + c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the X axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateX(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromXRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = c;
    out[6] = s;
    out[7] = 0;
    out[8] = 0;
    out[9] = -s;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Y axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateY(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromYRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = 0;
    out[2] = -s;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = s;
    out[9] = 0;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Z axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateZ(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromZRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = s;
    out[2] = 0;
    out[3] = 0;
    out[4] = -s;
    out[5] = c;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a quaternion rotation and vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslation = function (out, q, v) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - (yy + zz);
    out[1] = xy + wz;
    out[2] = xz - wy;
    out[3] = 0;
    out[4] = xy - wz;
    out[5] = 1 - (xx + zz);
    out[6] = yz + wx;
    out[7] = 0;
    out[8] = xz + wy;
    out[9] = yz - wx;
    out[10] = 1 - (xx + yy);
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Returns the translation vector component of a transformation
 *  matrix. If a matrix is built with fromRotationTranslation,
 *  the returned vector will be the same as the translation vector
 *  originally supplied.
 * @param  {vec3} out Vector to receive translation component
 * @param  {mat4} mat Matrix to be decomposed (input)
 * @return {vec3} out
 */
mat4.getTranslation = function (out, mat) {
    out[0] = mat[12];
    out[1] = mat[13];
    out[2] = mat[14];

    return out;
};

/**
 * Returns a quaternion representing the rotational component
 *  of a transformation matrix. If a matrix is built with
 *  fromRotationTranslation, the returned quaternion will be the
 *  same as the quaternion originally supplied.
 * @param {quat} out Quaternion to receive the rotation component
 * @param {mat4} mat Matrix to be decomposed (input)
 * @return {quat} out
 */
mat4.getRotation = function (out, mat) {
    // Algorithm taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    var trace = mat[0] + mat[5] + mat[10];
    var S = 0;

    if (trace > 0) {
        S = Math.sqrt(trace + 1.0) * 2;
        out[3] = 0.25 * S;
        out[0] = (mat[6] - mat[9]) / S;
        out[1] = (mat[8] - mat[2]) / S;
        out[2] = (mat[1] - mat[4]) / S;
    } else if (mat[0] > mat[5] & mat[0] > mat[10]) {
        S = Math.sqrt(1.0 + mat[0] - mat[5] - mat[10]) * 2;
        out[3] = (mat[6] - mat[9]) / S;
        out[0] = 0.25 * S;
        out[1] = (mat[1] + mat[4]) / S;
        out[2] = (mat[8] + mat[2]) / S;
    } else if (mat[5] > mat[10]) {
        S = Math.sqrt(1.0 + mat[5] - mat[0] - mat[10]) * 2;
        out[3] = (mat[8] - mat[2]) / S;
        out[0] = (mat[1] + mat[4]) / S;
        out[1] = 0.25 * S;
        out[2] = (mat[6] + mat[9]) / S;
    } else {
        S = Math.sqrt(1.0 + mat[10] - mat[0] - mat[5]) * 2;
        out[3] = (mat[1] - mat[4]) / S;
        out[0] = (mat[8] + mat[2]) / S;
        out[1] = (mat[6] + mat[9]) / S;
        out[2] = 0.25 * S;
    }

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScale = function (out, q, v, s) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale, rotating and scaling around the given origin
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     mat4.translate(dest, origin);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *     mat4.translate(dest, negativeOrigin);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @param {vec3} o The origin vector around which to scale and rotate
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScaleOrigin = function (out, q, v, s, o) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2],
        ox = o[0],
        oy = o[1],
        oz = o[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0] + ox - (out[0] * ox + out[4] * oy + out[8] * oz);
    out[13] = v[1] + oy - (out[1] * ox + out[5] * oy + out[9] * oz);
    out[14] = v[2] + oz - (out[2] * ox + out[6] * oy + out[10] * oz);
    out[15] = 1;

    return out;
};

/**
 * Calculates a 4x4 matrix from the given quaternion
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat} q Quaternion to create matrix from
 *
 * @returns {mat4} out
 */
mat4.fromQuat = function (out, q) {
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        yx = y * x2,
        yy = y * y2,
        zx = z * x2,
        zy = z * y2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - yy - zz;
    out[1] = yx + wz;
    out[2] = zx - wy;
    out[3] = 0;

    out[4] = yx - wz;
    out[5] = 1 - xx - zz;
    out[6] = zy + wx;
    out[7] = 0;

    out[8] = zx + wy;
    out[9] = zy - wx;
    out[10] = 1 - xx - yy;
    out[11] = 0;

    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;

    return out;
};

/**
 * Generates a frustum matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Number} left Left bound of the frustum
 * @param {Number} right Right bound of the frustum
 * @param {Number} bottom Bottom bound of the frustum
 * @param {Number} top Top bound of the frustum
 * @param {Number} near Near bound of the frustum
 * @param {Number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.frustum = function (out, left, right, bottom, top, near, far) {
    var rl = 1 / (right - left),
        tb = 1 / (top - bottom),
        nf = 1 / (near - far);
    out[0] = near * 2 * rl;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = near * 2 * tb;
    out[6] = 0;
    out[7] = 0;
    out[8] = (right + left) * rl;
    out[9] = (top + bottom) * tb;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = far * near * 2 * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspective = function (out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = 2 * far * near * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given field of view.
 * This is primarily useful for generating projection matrices to be used
 * with the still experiemental WebVR API.
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Object} fov Object containing the following values: upDegrees, downDegrees, leftDegrees, rightDegrees
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspectiveFromFieldOfView = function (out, fov, near, far) {
    var upTan = Math.tan(fov.upDegrees * Math.PI / 180.0),
        downTan = Math.tan(fov.downDegrees * Math.PI / 180.0),
        leftTan = Math.tan(fov.leftDegrees * Math.PI / 180.0),
        rightTan = Math.tan(fov.rightDegrees * Math.PI / 180.0),
        xScale = 2.0 / (leftTan + rightTan),
        yScale = 2.0 / (upTan + downTan);

    out[0] = xScale;
    out[1] = 0.0;
    out[2] = 0.0;
    out[3] = 0.0;
    out[4] = 0.0;
    out[5] = yScale;
    out[6] = 0.0;
    out[7] = 0.0;
    out[8] = -((leftTan - rightTan) * xScale * 0.5);
    out[9] = (upTan - downTan) * yScale * 0.5;
    out[10] = far / (near - far);
    out[11] = -1.0;
    out[12] = 0.0;
    out[13] = 0.0;
    out[14] = far * near / (near - far);
    out[15] = 0.0;
    return out;
};

/**
 * Generates a orthogonal projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} left Left bound of the frustum
 * @param {number} right Right bound of the frustum
 * @param {number} bottom Bottom bound of the frustum
 * @param {number} top Top bound of the frustum
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.ortho = function (out, left, right, bottom, top, near, far) {
    var lr = 1 / (left - right),
        bt = 1 / (bottom - top),
        nf = 1 / (near - far);
    out[0] = -2 * lr;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = -2 * bt;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 2 * nf;
    out[11] = 0;
    out[12] = (left + right) * lr;
    out[13] = (top + bottom) * bt;
    out[14] = (far + near) * nf;
    out[15] = 1;
    return out;
};

/**
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {vec3} eye Position of the viewer
 * @param {vec3} center Point the viewer is looking at
 * @param {vec3} up vec3 pointing up
 * @returns {mat4} out
 */
mat4.lookAt = function (out, eye, center, up) {
    var x0,
        x1,
        x2,
        y0,
        y1,
        y2,
        z0,
        z1,
        z2,
        len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

    if (Math.abs(eyex - centerx) < glMatrix.EPSILON && Math.abs(eyey - centery) < glMatrix.EPSILON && Math.abs(eyez - centerz) < glMatrix.EPSILON) {
        return mat4.identity(out);
    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1;
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2;
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;

    return out;
};

/**
 * Returns a string representation of a mat4
 *
 * @param {mat4} a matrix to represent as a string
 * @returns {String} string representation of the matrix
 */
mat4.str = function (a) {
    return 'mat4(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ', ' + a[4] + ', ' + a[5] + ', ' + a[6] + ', ' + a[7] + ', ' + a[8] + ', ' + a[9] + ', ' + a[10] + ', ' + a[11] + ', ' + a[12] + ', ' + a[13] + ', ' + a[14] + ', ' + a[15] + ')';
};

/**
 * Returns Frobenius norm of a mat4
 *
 * @param {mat4} a the matrix to calculate Frobenius norm of
 * @returns {Number} Frobenius norm
 */
mat4.frob = function (a) {
    return Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2) + Math.pow(a[9], 2) + Math.pow(a[10], 2) + Math.pow(a[11], 2) + Math.pow(a[12], 2) + Math.pow(a[13], 2) + Math.pow(a[14], 2) + Math.pow(a[15], 2));
};

/**
 * Adds two mat4's
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.add = function (out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    out[8] = a[8] + b[8];
    out[9] = a[9] + b[9];
    out[10] = a[10] + b[10];
    out[11] = a[11] + b[11];
    out[12] = a[12] + b[12];
    out[13] = a[13] + b[13];
    out[14] = a[14] + b[14];
    out[15] = a[15] + b[15];
    return out;
};

/**
 * Subtracts matrix b from matrix a
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.subtract = function (out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    out[6] = a[6] - b[6];
    out[7] = a[7] - b[7];
    out[8] = a[8] - b[8];
    out[9] = a[9] - b[9];
    out[10] = a[10] - b[10];
    out[11] = a[11] - b[11];
    out[12] = a[12] - b[12];
    out[13] = a[13] - b[13];
    out[14] = a[14] - b[14];
    out[15] = a[15] - b[15];
    return out;
};

/**
 * Alias for {@link mat4.subtract}
 * @function
 */
mat4.sub = mat4.subtract;

/**
 * Multiply each element of the matrix by a scalar.
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {Number} b amount to scale the matrix's elements by
 * @returns {mat4} out
 */
mat4.multiplyScalar = function (out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    out[8] = a[8] * b;
    out[9] = a[9] * b;
    out[10] = a[10] * b;
    out[11] = a[11] * b;
    out[12] = a[12] * b;
    out[13] = a[13] * b;
    out[14] = a[14] * b;
    out[15] = a[15] * b;
    return out;
};

/**
 * Adds two mat4's after multiplying each element of the second operand by a scalar value.
 *
 * @param {mat4} out the receiving vector
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @param {Number} scale the amount to scale b's elements by before adding
 * @returns {mat4} out
 */
mat4.multiplyScalarAndAdd = function (out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    out[3] = a[3] + b[3] * scale;
    out[4] = a[4] + b[4] * scale;
    out[5] = a[5] + b[5] * scale;
    out[6] = a[6] + b[6] * scale;
    out[7] = a[7] + b[7] * scale;
    out[8] = a[8] + b[8] * scale;
    out[9] = a[9] + b[9] * scale;
    out[10] = a[10] + b[10] * scale;
    out[11] = a[11] + b[11] * scale;
    out[12] = a[12] + b[12] * scale;
    out[13] = a[13] + b[13] * scale;
    out[14] = a[14] + b[14] * scale;
    out[15] = a[15] + b[15] * scale;
    return out;
};

/**
 * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ===)
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.exactEquals = function (a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2] && a[3] === b[3] && a[4] === b[4] && a[5] === b[5] && a[6] === b[6] && a[7] === b[7] && a[8] === b[8] && a[9] === b[9] && a[10] === b[10] && a[11] === b[11] && a[12] === b[12] && a[13] === b[13] && a[14] === b[14] && a[15] === b[15];
};

/**
 * Returns whether or not the matrices have approximately the same elements in the same position.
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.equals = function (a, b) {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2],
        a3 = a[3],
        a4 = a[4],
        a5 = a[5],
        a6 = a[6],
        a7 = a[7],
        a8 = a[8],
        a9 = a[9],
        a10 = a[10],
        a11 = a[11],
        a12 = a[12],
        a13 = a[13],
        a14 = a[14],
        a15 = a[15];

    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3],
        b4 = b[4],
        b5 = b[5],
        b6 = b[6],
        b7 = b[7],
        b8 = b[8],
        b9 = b[9],
        b10 = b[10],
        b11 = b[11],
        b12 = b[12],
        b13 = b[13],
        b14 = b[14],
        b15 = b[15];

    return Math.abs(a0 - b0) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a0), Math.abs(b0)) && Math.abs(a1 - b1) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a1), Math.abs(b1)) && Math.abs(a2 - b2) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a2), Math.abs(b2)) && Math.abs(a3 - b3) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a3), Math.abs(b3)) && Math.abs(a4 - b4) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a4), Math.abs(b4)) && Math.abs(a5 - b5) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a5), Math.abs(b5)) && Math.abs(a6 - b6) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a6), Math.abs(b6)) && Math.abs(a7 - b7) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a7), Math.abs(b7)) && Math.abs(a8 - b8) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a8), Math.abs(b8)) && Math.abs(a9 - b9) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a9), Math.abs(b9)) && Math.abs(a10 - b10) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a10), Math.abs(b10)) && Math.abs(a11 - b11) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a11), Math.abs(b11)) && Math.abs(a12 - b12) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a12), Math.abs(b12)) && Math.abs(a13 - b13) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a13), Math.abs(b13)) && Math.abs(a14 - b14) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a14), Math.abs(b14)) && Math.abs(a15 - b15) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a15), Math.abs(b15));
};

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class 3 Dimensional Vector
 * @name vec3
 */
var vec3 = {};

/**
 * Creates a new, empty vec3
 *
 * @returns {vec3} a new 3D vector
 */
vec3.create = function () {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    return out;
};

/**
 * Creates a new vec3 initialized with values from an existing vector
 *
 * @param {vec3} a vector to clone
 * @returns {vec3} a new 3D vector
 */
vec3.clone = function (a) {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
};

/**
 * Creates a new vec3 initialized with the given values
 *
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @returns {vec3} a new 3D vector
 */
vec3.fromValues = function (x, y, z) {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
};

/**
 * Copy the values from one vec3 to another
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the source vector
 * @returns {vec3} out
 */
vec3.copy = function (out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
};

/**
 * Set the components of a vec3 to the given values
 *
 * @param {vec3} out the receiving vector
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @returns {vec3} out
 */
vec3.set = function (out, x, y, z) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
};

/**
 * Adds two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.add = function (out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    return out;
};

/**
 * Subtracts vector b from vector a
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.subtract = function (out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    return out;
};

/**
 * Alias for {@link vec3.subtract}
 * @function
 */
vec3.sub = vec3.subtract;

/**
 * Multiplies two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.multiply = function (out, a, b) {
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
    return out;
};

/**
 * Alias for {@link vec3.multiply}
 * @function
 */
vec3.mul = vec3.multiply;

/**
 * Divides two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.divide = function (out, a, b) {
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
    return out;
};

/**
 * Alias for {@link vec3.divide}
 * @function
 */
vec3.div = vec3.divide;

/**
 * Math.ceil the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to ceil
 * @returns {vec3} out
 */
vec3.ceil = function (out, a) {
    out[0] = Math.ceil(a[0]);
    out[1] = Math.ceil(a[1]);
    out[2] = Math.ceil(a[2]);
    return out;
};

/**
 * Math.floor the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to floor
 * @returns {vec3} out
 */
vec3.floor = function (out, a) {
    out[0] = Math.floor(a[0]);
    out[1] = Math.floor(a[1]);
    out[2] = Math.floor(a[2]);
    return out;
};

/**
 * Returns the minimum of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.min = function (out, a, b) {
    out[0] = Math.min(a[0], b[0]);
    out[1] = Math.min(a[1], b[1]);
    out[2] = Math.min(a[2], b[2]);
    return out;
};

/**
 * Returns the maximum of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.max = function (out, a, b) {
    out[0] = Math.max(a[0], b[0]);
    out[1] = Math.max(a[1], b[1]);
    out[2] = Math.max(a[2], b[2]);
    return out;
};

/**
 * Math.round the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to round
 * @returns {vec3} out
 */
vec3.round = function (out, a) {
    out[0] = Math.round(a[0]);
    out[1] = Math.round(a[1]);
    out[2] = Math.round(a[2]);
    return out;
};

/**
 * Scales a vec3 by a scalar number
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to scale
 * @param {Number} b amount to scale the vector by
 * @returns {vec3} out
 */
vec3.scale = function (out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    return out;
};

/**
 * Adds two vec3's after scaling the second operand by a scalar value
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {Number} scale the amount to scale b by before adding
 * @returns {vec3} out
 */
vec3.scaleAndAdd = function (out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    return out;
};

/**
 * Calculates the euclidian distance between two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} distance between a and b
 */
vec3.distance = function (a, b) {
    var x = b[0] - a[0],
        y = b[1] - a[1],
        z = b[2] - a[2];
    return Math.sqrt(x * x + y * y + z * z);
};

/**
 * Alias for {@link vec3.distance}
 * @function
 */
vec3.dist = vec3.distance;

/**
 * Calculates the squared euclidian distance between two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} squared distance between a and b
 */
vec3.squaredDistance = function (a, b) {
    var x = b[0] - a[0],
        y = b[1] - a[1],
        z = b[2] - a[2];
    return x * x + y * y + z * z;
};

/**
 * Alias for {@link vec3.squaredDistance}
 * @function
 */
vec3.sqrDist = vec3.squaredDistance;

/**
 * Calculates the length of a vec3
 *
 * @param {vec3} a vector to calculate length of
 * @returns {Number} length of a
 */
vec3.length = function (a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    return Math.sqrt(x * x + y * y + z * z);
};

/**
 * Alias for {@link vec3.length}
 * @function
 */
vec3.len = vec3.length;

/**
 * Calculates the squared length of a vec3
 *
 * @param {vec3} a vector to calculate squared length of
 * @returns {Number} squared length of a
 */
vec3.squaredLength = function (a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    return x * x + y * y + z * z;
};

/**
 * Alias for {@link vec3.squaredLength}
 * @function
 */
vec3.sqrLen = vec3.squaredLength;

/**
 * Negates the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to negate
 * @returns {vec3} out
 */
vec3.negate = function (out, a) {
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    return out;
};

/**
 * Returns the inverse of the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to invert
 * @returns {vec3} out
 */
vec3.inverse = function (out, a) {
    out[0] = 1.0 / a[0];
    out[1] = 1.0 / a[1];
    out[2] = 1.0 / a[2];
    return out;
};

/**
 * Normalize a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to normalize
 * @returns {vec3} out
 */
vec3.normalize = function (out, a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    var len = x * x + y * y + z * z;
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len);
        out[0] = a[0] * len;
        out[1] = a[1] * len;
        out[2] = a[2] * len;
    }
    return out;
};

/**
 * Calculates the dot product of two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} dot product of a and b
 */
vec3.dot = function (a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
};

/**
 * Computes the cross product of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.cross = function (out, a, b) {
    var ax = a[0],
        ay = a[1],
        az = a[2],
        bx = b[0],
        by = b[1],
        bz = b[2];

    out[0] = ay * bz - az * by;
    out[1] = az * bx - ax * bz;
    out[2] = ax * by - ay * bx;
    return out;
};

/**
 * Performs a linear interpolation between two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.lerp = function (out, a, b, t) {
    var ax = a[0],
        ay = a[1],
        az = a[2];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    return out;
};

/**
 * Performs a hermite interpolation with two control points
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {vec3} c the third operand
 * @param {vec3} d the fourth operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.hermite = function (out, a, b, c, d, t) {
    var factorTimes2 = t * t,
        factor1 = factorTimes2 * (2 * t - 3) + 1,
        factor2 = factorTimes2 * (t - 2) + t,
        factor3 = factorTimes2 * (t - 1),
        factor4 = factorTimes2 * (3 - 2 * t);

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
};

/**
 * Performs a bezier interpolation with two control points
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {vec3} c the third operand
 * @param {vec3} d the fourth operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.bezier = function (out, a, b, c, d, t) {
    var inverseFactor = 1 - t,
        inverseFactorTimesTwo = inverseFactor * inverseFactor,
        factorTimes2 = t * t,
        factor1 = inverseFactorTimesTwo * inverseFactor,
        factor2 = 3 * t * inverseFactorTimesTwo,
        factor3 = 3 * factorTimes2 * inverseFactor,
        factor4 = factorTimes2 * t;

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
};

/**
 * Generates a random vector with the given scale
 *
 * @param {vec3} out the receiving vector
 * @param {Number} [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
 * @returns {vec3} out
 */
vec3.random = function (out, scale) {
    scale = scale || 1.0;

    var r = glMatrix.RANDOM() * 2.0 * Math.PI;
    var z = glMatrix.RANDOM() * 2.0 - 1.0;
    var zScale = Math.sqrt(1.0 - z * z) * scale;

    out[0] = Math.cos(r) * zScale;
    out[1] = Math.sin(r) * zScale;
    out[2] = z * scale;
    return out;
};

/**
 * Transforms the vec3 with a mat4.
 * 4th vector component is implicitly '1'
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {mat4} m matrix to transform with
 * @returns {vec3} out
 */
vec3.transformMat4 = function (out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2],
        w = m[3] * x + m[7] * y + m[11] * z + m[15];
    w = w || 1.0;
    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
    return out;
};

/**
 * Transforms the vec3 with a mat3.
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {mat4} m the 3x3 matrix to transform with
 * @returns {vec3} out
 */
vec3.transformMat3 = function (out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2];
    out[0] = x * m[0] + y * m[3] + z * m[6];
    out[1] = x * m[1] + y * m[4] + z * m[7];
    out[2] = x * m[2] + y * m[5] + z * m[8];
    return out;
};

/**
 * Transforms the vec3 with a quat
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {quat} q quaternion to transform with
 * @returns {vec3} out
 */
vec3.transformQuat = function (out, a, q) {
    // benchmarks: http://jsperf.com/quaternion-transform-vec3-implementations

    var x = a[0],
        y = a[1],
        z = a[2],
        qx = q[0],
        qy = q[1],
        qz = q[2],
        qw = q[3],


    // calculate quat * vec
    ix = qw * x + qy * z - qz * y,
        iy = qw * y + qz * x - qx * z,
        iz = qw * z + qx * y - qy * x,
        iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse quat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    return out;
};

/**
 * Rotate a 3D vector around the x-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateX = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0];
    r[1] = p[1] * Math.cos(c) - p[2] * Math.sin(c);
    r[2] = p[1] * Math.sin(c) + p[2] * Math.cos(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Rotate a 3D vector around the y-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateY = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[2] * Math.sin(c) + p[0] * Math.cos(c);
    r[1] = p[1];
    r[2] = p[2] * Math.cos(c) - p[0] * Math.sin(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Rotate a 3D vector around the z-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateZ = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0] * Math.cos(c) - p[1] * Math.sin(c);
    r[1] = p[0] * Math.sin(c) + p[1] * Math.cos(c);
    r[2] = p[2];

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Perform some operation over an array of vec3s.
 *
 * @param {Array} a the array of vectors to iterate over
 * @param {Number} stride Number of elements between the start of each vec3. If 0 assumes tightly packed
 * @param {Number} offset Number of elements to skip at the beginning of the array
 * @param {Number} count Number of vec3s to iterate over. If 0 iterates over entire array
 * @param {Function} fn Function to call for each vector in the array
 * @param {Object} [arg] additional argument to pass to fn
 * @returns {Array} a
 * @function
 */
vec3.forEach = function () {
    var vec = vec3.create();

    return function (a, stride, offset, count, fn, arg) {
        var i, l;
        if (!stride) {
            stride = 3;
        }

        if (!offset) {
            offset = 0;
        }

        if (count) {
            l = Math.min(count * stride + offset, a.length);
        } else {
            l = a.length;
        }

        for (i = offset; i < l; i += stride) {
            vec[0] = a[i];vec[1] = a[i + 1];vec[2] = a[i + 2];
            fn(vec, vec, arg);
            a[i] = vec[0];a[i + 1] = vec[1];a[i + 2] = vec[2];
        }

        return a;
    };
}();

/**
 * Get the angle between two 3D vectors
 * @param {vec3} a The first operand
 * @param {vec3} b The second operand
 * @returns {Number} The angle in radians
 */
vec3.angle = function (a, b) {

    var tempA = vec3.fromValues(a[0], a[1], a[2]);
    var tempB = vec3.fromValues(b[0], b[1], b[2]);

    vec3.normalize(tempA, tempA);
    vec3.normalize(tempB, tempB);

    var cosine = vec3.dot(tempA, tempB);

    if (cosine > 1.0) {
        return 0;
    } else {
        return Math.acos(cosine);
    }
};

/**
 * Returns a string representation of a vector
 *
 * @param {vec3} a vector to represent as a string
 * @returns {String} string representation of the vector
 */
vec3.str = function (a) {
    return 'vec3(' + a[0] + ', ' + a[1] + ', ' + a[2] + ')';
};

/**
 * Returns whether or not the vectors have exactly the same elements in the same position (when compared with ===)
 *
 * @param {vec3} a The first vector.
 * @param {vec3} b The second vector.
 * @returns {Boolean} True if the vectors are equal, false otherwise.
 */
vec3.exactEquals = function (a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2];
};

/**
 * Returns whether or not the vectors have approximately the same elements in the same position.
 *
 * @param {vec3} a The first vector.
 * @param {vec3} b The second vector.
 * @returns {Boolean} True if the vectors are equal, false otherwise.
 */
vec3.equals = function (a, b) {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2];
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2];
    return Math.abs(a0 - b0) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a0), Math.abs(b0)) && Math.abs(a1 - b1) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a1), Math.abs(b1)) && Math.abs(a2 - b2) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a2), Math.abs(b2));
};

/**
 * Creates a VertexAttributeObject aka VAO
 * @param gl a webgl context
 * @param useNative flag for whether or not to use native VAOs (which uses an extension for now)
 */
function createVAO(gl, useNative = true) {
    let vao = null;
    let ext = null;
    // TODO support cards that don't have this extension later
    if (useNative) {
        if (gl.hasOwnProperty('OES_vertex_array_object')) {
            ext = gl['OES_vertex_array_object'];
            vao = ext.createVertexArrayOES();
        } else {
            ext = gl.getExtension('OES_vertex_array_object');
            vao = ext.createVertexArrayOES();
        }
    }

    return {
        gl: gl,
        vao: vao,
        ext: ext,
        attributes: {},

        /**
         * Sets an attribute's location
         * @param shader {WebGLProgram} a WebGl shader program to associate with the attribute location
         * @param name {String} the name of the attribute
         * @param index {Number} an optional index. If null, will utilize the automatically assigned location
         * @returns {number} returns the location for the attribute
         */
        setAttributeLocation(shader, name, index = null) {
            let loc = 0;
            let gl = this.gl;

            // if we don't assign an index, get the automatically generated one
            if (index === null || index === undefined) {
                loc = gl.getAttribLocation(shader, name);
            } else {
                loc = gl.bindAttribLocation(shader, index, name);
            }
            return loc;
        },

        /**
         * Enable all of the attributes on a shader onto the VAO.
         * This will automatically set the attribute location to the order in which the
         * attribute was set in the shader settings, but will override that decision if the location index is
         * set in the attribute.
         *
         * @param shader a plane JS object that contains 3 things
         * 1. A WebGLProgram on the key "shader"
         * 2. an array at the key "attributes" that contains the name of all of the attributes we're looking for
         * as well as the size of each attribute.
         */
        enableAttributes(shader) {
            let gl = this.gl;
            let attribs = shader.attributes;
            for (let a in attribs) {
                let attrib = attribs[a];
                let attribLoc = this.attributes.length;

                // if the attribute has a location parameter, use that to set the attribute location,
                // otherwise use the next index in the attributes array
                if (attrib.hasOwnProperty('location')) {
                    attribLoc = attrib.location;
                }

                this.addAttribute(shader, attrib.name, attrib.size, attribLoc);
            }
            return this;
        },

        /**
         * Adds an attribute for the VAO to keep track of
         * @param shader {WebGLProgram} the shader that the attribute is a part of. Takes a WebGLProgram but also accepts a plain object created by the
         * {@link createShader} function
         * @param name {String} the name of the attribute to add/enable
         * @param size {Number} optional - the number of items that compose the attribute. For example, for something like, position, you might have xyz components, thus, 3 would be the size
         * @param location {Number} optional - the number to use as the attribute location. If it's not specified, will simply use it's index in the attributes object
         * @param setData {Boolean} optional - flag for whether or not to immediately run setData on the attribute. TODO enable by default
         * @param dataOptions {Object} optional - any options you might want to add when calling setData like an offset or stride value for the data
         * @returns {addAttribute}
         */
        addAttribute(shader, name, { size = 3, location, setData = false, dataOptions = {} } = {}) {
            let attribLoc = this.attributes.length;
            let webglProg = null;

            if (shader instanceof WebGLProgram) {
                webglProg = shader;
            } else {
                webglProg = shader.program;
            }

            // if location is undefined, just set attribute location
            // to be the next index in the attribute set.
            if (location === undefined) {
                attribLoc = location;
            }

            let attribLocation = this.setAttributeLocation(webglProg, name, attribLoc);
            this.attributes[name] = {
                loc: attribLocation,
                enabled: true,
                size: size
            };
            //enable the attribute
            this.enableAttribute(name);

            // if we want to just go ahead and set the data , run that
            this.setData(name, dataOptions);
            return this;
        },

        /**
         * Returns the location of the specified attribute
         * @param name {String} the name of the attribute.
         * @returns {*|number}
         */
        getAttribute(name) {
            return this.attributes[name].loc;
        },

        /**
         * Enables a vertex attribute
         * @param name {String} the name of the attribute you want to enable
         */
        enableAttribute(name) {
            // enable vertex attribute at the location
            this.gl.enableVertexAttribArray(this.attributes[name].loc);
        },

        /**
         * Disables a vertex attribute
         * @param name {String} the name of the vertex attribute to disable
         */
        disableAttribute(name) {
            this.gl.disableVertexAttribArray(this.attributes[name].loc);
        },

        /**
         * Shorthand for calling gl.vertexAttribPointer. Essentially sets the data into the vao for the
         * currently bound buffer.
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        setData: function (name, { options } = {}) {
            let loc = this.attributes[name].loc;
            let size = this.attributes[name].size;

            let pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },
        /**
         * Shorthand for calling gl.vertexAttribPointer
         * @deprecated gonna start using setData instead so it's a little more clear as to what this is doing
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        point(name, { options } = {}) {
            let loc = this.attributes[name].loc;
            let size = this.attributes[name].size;

            let pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },

        /**
         * Binds the vao
         */
        bind() {
            ext.bindVertexArrayOES(this.vao);
        },

        /**
         * Unbinds the vao
         */
        unbind() {
            ext.bindVertexArrayOES(null);
        }
    };
}

/**
 * Simple function to create a VBO aka buffer
 * @param gl a WebGLRendering context
 * @param data the information for the buffer. If it's a regular array, it'll be turned into a TypedArray
 * @param bufferType the type of buffer it is. By default, it's an ARRAY_BUFFER
 * @param usage the usage for the buffer. by default it's STATIC_DRAW
 */
function createVBO(gl, { data = null, indexed = false, usage } = {}) {
    let buffer = null;

    // set the buffer type
    let bufferType = "ARRAY_BUFFER";
    if (indexed === true) {
        bufferType = "ELEMENT_ARRAY_BUFFER";
    }
    let name = bufferType;
    bufferType = gl[bufferType];

    // set the usage
    usage = usage || "STATIC_DRAW";
    usage = gl[usage];
    buffer = gl.createBuffer();

    var obj = {
        gl: gl,
        buffer: buffer,
        bufferTypeName: name,
        type: bufferType,
        usage: usage,

        raw() {
            return this.buffer;
        },
        /**
         * Updates the buffer with new information
         * @param data a array of some kind containing your new data
         */
        updateBuffer(data) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.bind();
            this.gl.bufferSubData(this.type, 0, data);
            this.unbind();
        },
        /**
         * Sets data onto the vbo.
         * @param data the data for the vbo. Can either be a regular array or a typed array.
         * If a regular array is used, will determine buffer type based on the settings.
         */
        bufferData(data, updatedData = false) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.gl.bufferData(this.type, data, usage);
        },
        bind() {
            this.gl.bindBuffer(this.type, this.buffer);
        },
        unbind() {
            this.gl.bindBuffer(this.type, null);
        }
    };

    if (data !== null) {
        obj.bind();
        obj.bufferData(data);
        obj.unbind();
    }

    return obj;
}

class Mesh {
    constructor(gl) {
        this.gl = gl;
        this.model = mat4.create();
        this.rotation = {
            x: 0,
            y: 0,
            z: 0
        };
        this.rotateAxis = vec3.create();
        vec3.set(this.rotateAxis, this.rotation.x, this.rotation.y, this.rotation.z);
        this.scale = vec3.create();
        this.position = vec3.create();
        this.vao = createVAO(gl);
    }

    /**
     * Adds an attribute to the mesh
     * @param shader {Object} either a WebGLShader program or a object created with createShader
     * @param name {String} the name of the attribute in the shader
     * @param data {TypedArray} data associated with the attribute
     */
    addAttribute(shader, name, data) {
        let buffer = createVBO(this.gl);
        this.vao.bind();
        buffer.bind();
        buffer.bufferData(data);
        this.vao.addAttribute(shader, name);
        buffer.unbind();
        this.vao.unbind();
    }
    translate(x = 1, y = 1, z = 0) {
        vec3.set(this.position, x, y, z);
        mat4.translate(this.model, this.model, this.position);
    }

    scaleModel(x = 1, y = 1, z = 1) {
        vec3.set(this.scale, x, y, z);
        mat4.scale(this.model, this.model, this.scale);
    }

    rotateX(angle) {
        mat4.rotateX(this.model, this.model, angle);
    }

    rotateY(angle) {
        mat4.rotateY(this.model, this.model, angle);
    }

    rotateZ(angle) {
        mat4.rotateZ(this.model, this.model, angle);
    }
}

/**
 * Compiles either a fragment or vertex shader
 * @param gl a webgl context
 * @param type the type of shader. Should be either gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
 * @param source the source (as a string) for the shader
 * @returns {*} returns the compiled shader
 */
function compileShader(gl, type, source) {
    let shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error("Error in shader compilation - " + gl.getShaderInfoLog(shader));
        return false;
    } else {
        return shader;
    }
}

/**
 * The main function for creating a shader. Shader also manages figuring out
 * attribute and uniform location indices.
 *
 * @param gl a webgl context
 * @param vertex the source for the vertex shader
 * @param fragment the source for the fragment shader
 * @returns {*} returns the WebGLProgram compiled from the two shaders
 */
function makeShader(gl, vertex, fragment) {
    let vShader = compileShader(gl, gl.VERTEX_SHADER, vertex);
    let fShader = compileShader(gl, gl.FRAGMENT_SHADER, fragment);

    if (vShader !== false && fShader !== false) {
        let program = gl.createProgram();
        gl.attachShader(program, vShader);
        gl.attachShader(program, fShader);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            logError("Could not initialize WebGLProgram");
            throw "Couldn't link shader program - " + gl.getProgramInfoLog(program);
            return false;
        } else {
            return program;
        }
    }
}
/**
 * A function to quickly setup a WebGL shader program.
 * Modeled a bit after thi.ng
 * @param gl the webgl context to use
 * @param spec a object containing the out line of what the shader would look like.
 * @returns {*} and JS object with the shader information along with some helpful functions
 */
function createShader(gl = null, spec) {
    let vs = null;
    let fs = null;
    let uniforms = {};
    let attributes = {};
    let precision = spec.precision !== undefined ? spec.precision : "highp";
    if (gl === null) {
        console.error("");
        return false;
    }

    if (!spec.hasOwnProperty("vertex") || !spec.hasOwnProperty("fragment")) {
        logError("spec does not contain vertex and/or fragment shader");
        return false;
    }

    // if either of the shader sources are arrays, run the compile shader function
    if (spec.vertex instanceof Array) {
        spec.vertex = compileShaderSource(spec.vertex);
    }

    if (spec.fragment instanceof Array) {
        spec.fragment = `precision ${ highp } float;` + compileShaderSource(spec.fragment);
    }

    // build the shader
    let shader = makeShader(gl, spec.vertex, spec.fragment);

    // set uniforms and their locations (plus default values if specified)
    if (spec.hasOwnProperty('uniforms')) {

        let uValues = spec.uniforms.map(value => {
            if (typeof value === 'string') {
                let loc = gl.getUniformLocation(shader, value);
                uniforms[value] = loc;
            } else if (typeof value === 'object') {
                // TODO make sure to set default uniform value if present
                let loc = gl.getUniformLocation(shader, value.name);
                uniforms[value.name] = loc;
            }
        });
    }

    /**
     * Arranges all of the attribute data into neat containers
     * to allow for easy processing by a VAO.
     * Attributes should be specified as arrays
     */
    if (spec.hasOwnProperty('attributes')) {
        let attribs = spec.attributes.map(value => {

            attributes[value[0]] = {
                size: value[1],
                name: value[0]
            };

            // if a desired uniform location is set ,
            // make sure to reflect that in the information
            if (value[2] !== undefined) {
                attributes[value[0]].location = value[2];
            }
        });
    }

    return {
        gl: gl,
        program: shader,
        uniforms: uniforms,
        attributes: attributes,
        /**
         * Binds the shader for use
         */
        bind() {
            this.gl.useProgram(this.program);
        },
        /**
         * Sets a matrix uniform for a 4x4 matrix
         * @param name the name of the uniform whose value you want to set.
         */
        setMatrixUniform(name, value) {

            this.gl.uniformMatrix4fv(this.uniforms[name], false, value);
        },
        /**
         * Sets the uniform value for a texture. Optionally
         * @param value
         */
        setTextureUniform(name, value) {
            this.gl.uniform1i(this.uniforms[name], value);
        },

        getUniform(name) {
            return this.uniforms[name];
        },

        /**
         * Sends a uniform to the currently bound shader. Attempts to derrive
         * the correct uniform function to use
         * @param name {String} name of the uniform
         * @param value {*} the value to send to the uniform
         */
        uniform(name, value) {

            // if the value is a 4x4 matrix (assuming from gl-matrix)
            if (value.length !== undefined && value.length === 16) {
                this.setMatrixUniform(name, value);
            } else if (value.length !== undefined && value.length === 3) {
                this.gl.uniform3fv(this.uniforms[name], value);
            } else {
                // send a float based value
                this.gl.uniform1f(this.uniforms[name], value);
            }
        }

    };
}

/**
 * Allows you to compile multiple shader sources into one file.
 * Keep in mind this does not distinguish between vertex and fragment and will not
 * insert things like precision specifiers and/or extensions
 * @param sources
 */
function compileShaderSource(...sources) {
    if (sources[0] instanceof Array) {
        var s = sources[0].map(source => {
            return source + "\n";
        });
        return s.join("");
    } else {
        var s = sources.map(source => {
            return source + "\n";
        });
        return s.join("");
    }
}

/**
 * Flattens an nested array that is assumed to be nested with child arrays used in place of
 * an actual vector object. Note, this does not check for completeness and will automatically
 * only take the first 3 values of the child arrays
 * @param array the parent array
 * @returns {Array}
 */
function flattenArray(array) {

    let fin = [];
    let len = array.length;
    for (var i = 0; i < len; ++i) {
        let arr = array[i];
        fin.push(arr[0], arr[1], arr[2]);
    }
    return fin;
}

/**
 * Does subtraction between two arrays. Assumes both arrays have 3 values each inside
 * @param array1 {Array} the array to subtract from
 * @param array2 {Array} the array to subtract
 * @returns {*[]}
 */






/**
 * Creates an array with a range of values
 * @param from {Number} the value to start from
 * @param to {Number} the value end at.
 * @returns {Array}
 */


/**
 * Returns a random vec3(in the form of an array)
 * @returns {*[]}
 */


/**
 * Very simple array cloning util.
 * Note - only works with arrays who have 3 elements
 * @param arrayToClone the array to clone
 * @returns {*[]} the new array
 */




/**
 * ensures that when using an array as a 3d vector, that it actually
 * contains only 3 components.
 * @param array the array to verify
 * @returns {*}
 */

// port of the Three.js explode modifier.
function explode(geo) {
    var vertices = [];

    for (var i = 0, il = geo.cells.length; i < il; i++) {

        var n = vertices.length;
        var face = geo.cells[i];
        var a = face[0];
        var b = face[1];
        var c = face[2];

        var va = geo.positions[a];
        var vb = geo.positions[b];
        var vc = geo.positions[c];

        vertices.push([va[0], va[1], va[2]]);
        vertices.push([vb[0], vb[1], vb[2]]);
        vertices.push([vc[0], vc[1], vc[2]]);

        face[0] = n;
        face[1] = n + 1;
        face[2] = n + 2;
    }

    geo.positions = vertices;
    return geo;
}

var vert = "\nuniform mat4 viewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 modelMatrix;\nuniform sampler2D amplitude;\nattribute vec3 position;\nattribute vec3 displacement;\nattribute vec3 normals;\nvarying vec3 vDisplacement;\nvarying vec3 vPosition;\nvoid main(){\n    vDisplacement = displacement;\n    vec4 amp = texture2D(amplitude,position.xy);\n    vec3 pos = position + normals * displacement * amp.xyz;\n   vPosition = pos;\n    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos,1.);\n}";

var frag = "precision highp float;\nuniform vec2 iResolution;\nuniform sampler2D amplitude;\nvarying vec3 vDisplacement;\nvarying vec3 vPosition;\nuniform float time;\nvoid main(){\n    vec4 amp = texture2D(amplitude,gl_FragCoord.xy);\n    vec2 u = gl_FragCoord.xy;\n    u /= iResolution.xy;\n    vec3 color = vec3(0.);\n    color.xy = .5 - u;\n    float t = time;\n    float z = atan(color.y,color.x) + 3.;\n    float v = cos(z + sin(t * .1)) + 0.5 * cos(u.x * 10. * 1.3) * amp.z;\n    color.x = 1.2 + sin(z + t * .2) + sin(u.y*10.+t*1.5)*.5;\n \tcolor.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + color.x*.5;\n    color.xy * amp.xy * amp.yz;\n \tcolor *= vPosition * vDisplacement;\n    gl_FragColor = vec4(color,amp.z);\n}";

var normalizeNd = createCommonjsModule(function (module) {
  module.exports = normalize;

  function normalize(vec) {
    var mag = 0;
    for (var n = 0; n < vec.length; n++) {
      mag += vec[n] * vec[n];
    }
    mag = Math.sqrt(mag);

    // avoid dividing by zero
    if (mag === 0) {
      return Array.apply(null, new Array(vec.length)).map(Number.prototype.valueOf, 0);
    }

    for (var n = 0; n < vec.length; n++) {
      vec[n] /= mag;
    }

    return vec;
  }
});

var index$22 = createCommonjsModule(function (module) {
  var normalize = normalizeNd;

  module.exports = icosphere;

  function icosphere(subdivisions) {
    subdivisions = +subdivisions | 0;

    var positions = [];
    var faces = [];
    var t = 0.5 + Math.sqrt(5) / 2;

    positions.push([-1, +t, 0]);
    positions.push([+1, +t, 0]);
    positions.push([-1, -t, 0]);
    positions.push([+1, -t, 0]);

    positions.push([0, -1, +t]);
    positions.push([0, +1, +t]);
    positions.push([0, -1, -t]);
    positions.push([0, +1, -t]);

    positions.push([+t, 0, -1]);
    positions.push([+t, 0, +1]);
    positions.push([-t, 0, -1]);
    positions.push([-t, 0, +1]);

    faces.push([0, 11, 5]);
    faces.push([0, 5, 1]);
    faces.push([0, 1, 7]);
    faces.push([0, 7, 10]);
    faces.push([0, 10, 11]);

    faces.push([1, 5, 9]);
    faces.push([5, 11, 4]);
    faces.push([11, 10, 2]);
    faces.push([10, 7, 6]);
    faces.push([7, 1, 8]);

    faces.push([3, 9, 4]);
    faces.push([3, 4, 2]);
    faces.push([3, 2, 6]);
    faces.push([3, 6, 8]);
    faces.push([3, 8, 9]);

    faces.push([4, 9, 5]);
    faces.push([2, 4, 11]);
    faces.push([6, 2, 10]);
    faces.push([8, 6, 7]);
    faces.push([9, 8, 1]);

    var complex = {
      cells: faces,
      positions: positions
    };

    while (subdivisions-- > 0) {
      complex = subdivide(complex);
    }

    positions = complex.positions;
    for (var i = 0; i < positions.length; i++) {
      normalize(positions[i]);
    }

    return complex;
  }

  // TODO: work out the second half of loop subdivision
  // and extract this into its own module.
  function subdivide(complex) {
    var positions = complex.positions;
    var cells = complex.cells;

    var newCells = [];
    var newPositions = [];
    var midpoints = {};
    var f = [0, 1, 2];
    var l = 0;

    for (var i = 0; i < cells.length; i++) {
      var cell = cells[i];
      var c0 = cell[0];
      var c1 = cell[1];
      var c2 = cell[2];
      var v0 = positions[c0];
      var v1 = positions[c1];
      var v2 = positions[c2];

      var a = getMidpoint(v0, v1);
      var b = getMidpoint(v1, v2);
      var c = getMidpoint(v2, v0);

      var ai = newPositions.indexOf(a);
      if (ai === -1) ai = l++, newPositions.push(a);
      var bi = newPositions.indexOf(b);
      if (bi === -1) bi = l++, newPositions.push(b);
      var ci = newPositions.indexOf(c);
      if (ci === -1) ci = l++, newPositions.push(c);

      var v0i = newPositions.indexOf(v0);
      if (v0i === -1) v0i = l++, newPositions.push(v0);
      var v1i = newPositions.indexOf(v1);
      if (v1i === -1) v1i = l++, newPositions.push(v1);
      var v2i = newPositions.indexOf(v2);
      if (v2i === -1) v2i = l++, newPositions.push(v2);

      newCells.push([v0i, ai, ci]);
      newCells.push([v1i, bi, ai]);
      newCells.push([v2i, ci, bi]);
      newCells.push([ai, bi, ci]);
    }

    return {
      cells: newCells,
      positions: newPositions
    };

    // reuse midpoint vertices between iterations.
    // Otherwise, there'll be duplicate vertices in the final
    // mesh, resulting in sharp edges.
    function getMidpoint(a, b) {
      var point = midpoint(a, b);
      var pointKey = pointToKey(point);
      var cachedPoint = midpoints[pointKey];
      if (cachedPoint) {
        return cachedPoint;
      } else {
        return midpoints[pointKey] = point;
      }
    }

    function pointToKey(point) {
      return point[0].toPrecision(6) + ',' + point[1].toPrecision(6) + ',' + point[2].toPrecision(6);
    }

    function midpoint(a, b) {
      return [(a[0] + b[0]) / 2, (a[1] + b[1]) / 2, (a[2] + b[2]) / 2];
    }
  }
});

//import {createIco} from "./ico"
class Speaker extends Mesh {
    constructor(gl) {
        super(gl);

        this.shader = createShader(gl, {
            vertex: vert,
            fragment: frag,
            attributes: [['position', 3], ['displacement', 3]],
            uniforms: ['projectionMatrix', 'viewMatrix', 'modelMatrix', 'amplitude', 'iResolution', 'time']
        });
        this.time = 0.0;
        this.resolution = new Float32Array([window.innerWidth, window.innerHeight]);

        this._build();
    }

    _build() {
        let gl = this.gl;
        let data = index$22(2);

        data = explode(data);
        let positionData = flattenArray(data.positions);
        let normalsData = positionData.slice(0);
        let length = positionData.length;

        // build buffers
        let displacement = createVBO(gl);
        let positions = createVBO(gl);
        let normals = createVBO(gl);
        let idx = createVBO(gl, {
            indexed: true
        });

        this.vao.bind();

        // buffer positions
        positions.bind();
        positions.bufferData(positionData);
        this.vao.addAttribute(this.shader, 'position', {
            setData: true
        });

        // buffer indices
        idx.bind();
        idx.bufferData(flattenArray(data.cells));

        displacement.bind();
        var displaceData = [];
        var numFaces = data.cells.length;
        for (var f = 0; f < numFaces; ++f) {
            var index = 9 * f;
            var d = 10 * (0.5 - Math.random());

            for (var i = 0; i < 3; ++i) {
                displaceData[index + 3 * i] = d;
                displaceData[index + 3 * i + 1] = d;
                displaceData[index + 3 * i + 2] = d;
            }
        }
        displacement.bufferData(displaceData);
        this.vao.addAttribute(this.shader, 'displacement', {
            setData: true
        });

        this.vao.unbind();
        displacement.unbind();
        positions.unbind();
        idx.unbind();
        this.num = data.cells.length * 3;

        // calculate normals for everything
        let normalLength = normalsData.length;
        for (var i = 0; i < normalLength; i += 3) {
            normals[i] = Math.random();
            normals[i + 1] = Math.random();
            normals[i + 2] = Math.random();
        }
        this.vao.bind();
        normals.bind();
        normals.bufferData(normalsData);
        this.vao.addAttribute(this.shader, 'normals');

        this.vao.unbind();
        normals.unbind();
        // scale the base shape so we can see it.
        this.scaleModel(5, 5, 5);
    }

    draw(camera) {
        var dt = Date.now() * 0.001;
        this.time += 0.1;
        let gl = this.gl;
        gl.disable(gl.DEPTH_TEST);
        this.shader.bind();
        analyser.bindFrequencies(0);
        this.shader.setMatrixUniform('projectionMatrix', camera.projection);
        this.shader.setMatrixUniform('viewMatrix', camera.view);
        this.shader.setMatrixUniform('modelMatrix', this.model);

        this.shader.uniform('time', this.time);
        this.gl.uniform2fv(this.shader.getUniform('iResolution'), this.resolution);
        this.rotateY(0.);
        this.rotateZ(0.02);
        this.rotateX(0.02);

        this.vao.bind();
        this.gl.drawElements(this.gl.TRIANGLES, this.num, UNSIGNED_SHORT, 0);
        this.vao.unbind();

        this.gl.bindTexture(TEXTURE_2D, null);
    }

}

var quadvert = "\nattribute vec3 position;\nvarying vec2 uv;\nconst vec2 scale = vec2(0.5,0.5);\nvoid main(){\n    uv = position.xy * scale + scale;\n    gl_Position = vec4(position,1.0);\n}";

var quadfrag = "precision highp float;\n#ifdef USE_TEXTURE\n    uniform sampler2D debugTex;\n#endif\nvarying vec2 uv;\nvoid main(){\n  #ifdef USE_TEXTURE\n     vec4 dat = texture2D(debugTex,uv);\n     gl_FragColor = dat;\n  #else\n     gl_FragColor = vec4(1.);\n  #endif\n}";

/**
 * Quickly creates a quad usable in a variety of ways.
 * @param gl {WebGLRenderingContext}
 * @param withTexture {boolean} a boolean value indicating whether or not you're trying to create a rendering quad. If you are, pass in true and instead of a color
 * things will get set up to draw a texture instead.
 * @param fragmentShader {String} optional fragment shader, primarily used for ping-ponging data between textures.
 * @returns {{vao: ({gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*), shader: ({gl, program, uniforms, attributes, bind, setMatrixUniform, setTextureUniform, uniform}|*), buffer: *, hasTexture: boolean, gl: *, draw: draw}}
 */
function createQuad(gl, { withTexture = false, fragmentShader = false, uniformMap = [], vertexShader = false } = {}) {
    let frag = quadfrag;
    let vertex = quadvert;
    let uniforms = ['debugTex'];
    let vertices = [1.0, 1.0, 0.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, -1.0, -1.0, 0.0];

    let vao = createVAO(gl);
    vao.bind();

    // if uniform map is not empty, merge with default map
    if (uniformMap.length > 0) {
        uniformMap.forEach(obj => {
            uniforms.push(obj);
        });
    }

    // if we don't have our own fragment shader, make sure to inject
    // the texture define statement if we need to support a texture.
    if (!fragmentShader) {
        if (withTexture) {
            frag = "#define USE_TEXTURE\n" + quadfrag;
        }
    } else {
        frag = fragmentShader;
    }

    // if we aren't using the default vertex shader
    if (vertexShader) {
        vertex = vertexShader;
    }

    let shader = createShader(gl, {
        vertex: vertex,
        fragment: frag,
        attributes: [['position', 3]],
        uniforms: uniforms
    });
    // buffer data onto the buffer
    let buffer = createVBO(gl);

    // enable attributes
    buffer.bind();
    buffer.bufferData(vertices);
    vao.addAttribute(shader, 'position');
    vao.setData('position');
    buffer.unbind();
    vao.unbind();

    return {
        vao: vao,
        shader: shader,
        buffer: buffer,
        type: "quad",
        hasTexture: withTexture,
        gl: gl,
        drawWithCallback(cb) {
            this.vao.bind();
            this.shader.bind();

            this.vao.enableAttribute('position');
            cb(shader);

            this.gl.drawArrays(TRIANGLE_STRIP, 0, 4);
            this.vao.disableAttribute('position');
            this.vao.unbind();
        },
        draw(textureUnit = 0) {

            this.vao.bind();
            this.shader.bind();

            this.vao.enableAttribute('position');

            if (this.hasTexture) {
                this.shader.setTextureUniform('debugTex', textureUnit);
            }

            this.gl.drawArrays(TRIANGLE_STRIP, 0, 4);
            this.vao.disableAttribute('position');
            this.vao.unbind();
        }
    };
}

/**
 * Takes a Quad object made with `createQuad` and draws it
 * @param quad
 */

// background for everything
class Background {
    constructor(gl, fragment) {
        this.gl = gl;
        this.quad = createQuad(gl, {
            fragmentShader: fragment,
            uniformMap: ['time', 'resolution', 'audioTexture']
        });

        this.time = 0.0;
        this.resolution = new Float32Array([window.innerWidth, window.innerHeight]);
    }

    draw() {
        this.time += 0.01;
        var gl = this.gl;
        this.gl.enable(this.gl.DEPTH_TEST);
        var resolution = this.resolution;
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

        this.quad.drawWithCallback(shader => {
            analyser.bindFrequencies(0);
            shader.uniform('time', this.time);
            shader.setTextureUniform('audioTexture', 0);
            gl.uniform2fv(shader.getUniform('resolution'), resolution);
        });
    }
}

/**
 * Load audio file for playback
 * @param path path to file
 * @param cb callback for when it's ok to start playing the audio
 */





function loadAudioSource(gl, url, cb) {
    let audioCtx = new (window.AudioContext || window.webkitAudioContext)();
    let req = new XMLHttpRequest();

    let source = audioCtx.createBufferSource();

    req.open('GET', url, true);

    req.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
    req.responseType = 'arraybuffer';
    req.onload = () => {
        let audioData = req.response;
        audioCtx.decodeAudioData(audioData, buffer => {
            // this is hacky but necessary for play/pause
            window.audioBuffer = buffer;

            source.buffer = buffer;
            source.connect(audioCtx.destination);

            var analyser = GLAudioAnalyser(gl, source, audioCtx);
            cb(source, analyser);
        }, e => {
            console.log("error decoding - ", e.err);
        });
    };

    req.send();
}

var background = "\nprecision highp float;\nuniform vec2 resolution;\nuniform float time;\nuniform sampler2D audioTexture;\nmat2 mm2(in float a){float c = sin(a), s = cos(a);return mat2(c,-s,s,c);}\nfloat aspect = resolution.x/resolution.y;\nfloat featureSize = 60./((resolution.x*aspect+resolution.y));\nvarying vec2 uv;\nvoid main(){\n    vec2 p = gl_FragCoord.xy / resolution.xy * 4.5 - 3.0;\n    vec4 dat = texture2D(audioTexture,gl_FragCoord.xy);\n\tvec3 yellow = vec3(mix(dat.x,dat.y,1.0) + dat.x,mix(dat.z,dat.x,1.0) + dat.y,sin(time) * cos(dat.x));\n\tyellow.y += sin( 0.11 * 1.5) * dat.z * cos(time);\n\tvec2 t = mm2(featureSize) * vec2(1.);\n\tyellow.xy *= t * sin(time);\n\tvec2 pa = vec2(abs(yellow.x - .9),abs(yellow.y));\n    gl_FragColor =vec4(yellow,dat.z) + vec4(pa,0.0,dat.x);\n}\n";

// ============= BUILD CORE STUFF =============
// renderer
const gl = createRenderer().setFullscreen().attachToScreen();

// basic camera
const camera = index$1({
    fov: Math.PI / 4,
    position: [0, 0, 5],
    near: 0.1,
    far: 10000,
    viewport: [0, 0, window.innerWidth, window.innerHeight]
});

//============ UI =============


//============ Objects =============//
//var smoke = new SmokeParticles(gl);
var speaker = new Speaker(gl);
var b = new Background(gl, background);
//var url = "https://soundcloud.com/the-ting-tings/wrong-club-us-radio-edit";
var url = "/audio/daijyobanai.mp3";
loadAudioSource(gl, url, (audio, analyser) => {
    window.analyser = analyser;
    setTimeout(function () {
        audio.start(0);
    });
    animate();
});
//oadAudio(gl,url, (audio,analyser) => {
//   window.analyser = analyser;
//   animate();
//)
//============ ANIMATE =============

var time = 0.0;
function animate() {
    time += 0.1;
    window.time = time;

    gl.clearScreen();

    b.draw();
    speaker.draw(camera);
    // eh... not sure what to do with this :p
    //smoke.update();
    //smoke.draw(camera)

    requestAnimationFrame(animate);
}

})));
