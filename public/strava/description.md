I was very fortunate to be a part of a great team of folks working on a new home/feature area for Strava.com.
The whole site is a complete redesign of the look and feel utilizing a wide variety of modern techniques including some rather unconvential methods for building out responsive animations. 
It should be live now at [strava.com](https://www.strava.com)