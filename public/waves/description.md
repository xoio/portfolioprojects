This was an exercise in learning about Oscilation and movement while reading [Dan Shiffman's](http://shiffman.net/) excellent [Nature of Code](http://natureofcode.com/) book. I thought it could be an 
interesting undertaking to port the idea to WebGL, as well as add in some other effects like post processing which was done using 
[Jaume Sanchez's](https://www.clicktorelease.com/) excellent [Wagner](https://github.com/spite/Wagner) postprocessing library.
You can see it running [here](https://xoio.gitlab.io/projects/waves/)